public class Employee implements CompanyRules // Implement Interface in class 
{
	//final int age;
	
	public static void main(String [] args)
	{
		Employee emp = new Employee(); // object creating
		//Employee.comeOnTime(); // using class name to call the interface method but it will show error because the interface method is non-static
		//System.out.println(Rules.minSalary); // using class name to call the interface final and static variable 
		//System.out.println(emp.minSalary); // using object name to call the final and static variable
		//emp.no_Of_Leaves = 20; // reassiging the interface variable but it will show the error because the interface variabe is defaut by final and static.
		//Rules.comeOnTime(); //using class name to call the interface method but it will show error because the interface method is non-static
		emp.idCard(); //object to call the override the interface method  
		
		
		
	}
	
	public void comeOnTime() // The interface method is defined
	{
		System.out.println("Come on Time");
	}
	
	public void takeLeave() // The interface method is defined
	{
		System.out.println("Take Leave");
		
	}
	
	public void setSalary() // The interface method is defined
	{
		System.out.println("Set Salary");
		
	}

}