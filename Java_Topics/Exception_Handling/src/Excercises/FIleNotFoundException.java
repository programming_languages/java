package Excercises;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FIleNotFoundException 
{
    public static void main(String[] args) throws IOException 
    {
        String path = "/home/karkuvel/Documents/Programming_Languages/java/Java_Topics/Exception_Handling/src/File/Praba.txt";
        fileNotFoundException(path);
    }
    public static void fileNotFoundException(String path) throws IOException
    {
        File note = new File(path);
        System.out.println(note.exists());
        FileReader reader = new FileReader(note);
        BufferedReader br = new BufferedReader(reader);
        String st = br.readLine();
        System.out.println(st.toString());
        int count =0;
        while(st  != null)
        {
            System.out.println(st);
            st = br.readLine();
            count++;
        }
        System.out.println(count);
        
    }

}
