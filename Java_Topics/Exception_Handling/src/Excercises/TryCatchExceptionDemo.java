package Excercises;

import java.util.Scanner;

public class TryCatchExceptionDemo 
{
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) 
    {
        div();
    }
    public static int div()
    {
        int sum = 0;
        System.out.println("Enter The two Number to div");
        int no1 = scanner.nextInt();
        int no2 = scanner.nextInt();
        try
        {
            sum = no1 / no2;
        }
        catch(ArithmeticException ae)
        {
            ae.printStackTrace();
            div();
        }
        return sum;
    }
}
