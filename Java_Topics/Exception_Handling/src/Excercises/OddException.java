package Excercises;

public class OddException 
{
    public void checkOddEven(int no)
    {
        if(no % 2 != 0)
        {
            throw new IllegalArgumentException("You Enter a Odd Number");
        }
        else 
        {
            System.out.println("It is Even Number");
        }
    }
}
