package com.indianBank.loan; //Creating directory or flolder using package


public class InterestCalculation
{
	public int interest = 5;
  	public static String name = "IOB";
	
  
	public InterestCalculation() //No-args Constructor
  	{
		
    	System.out.println("InterestCalculation-CONST"); // print statement
  	
	}
	public static void main(String[] args)
	{
  		
		//private int mb = 500;
  		
		//System.out.println("Indian Bank");
  		
		//System.out.println(10);
  		
		InterestCalculation ic = new InterestCalculation(); // Object Creating
  		
		ic.calculate(); //Metheod calling
  		
		System.out.println(ic.interest); //print instance variable using object
		
		System.out.println(InterestCalculation.name); //print class or static variable using class name
	}
	public void calculate() // Method Defining
	{
		
  		System.out.println("Interest-calculate");
	
	}

}