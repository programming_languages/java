											Access Modifier

What is Access Modifier ?
	Access specifiers (also known as access modifiers) are keywords used to control the visibility and accessibility of classes, methods, fields, and other members within a Java program. 

Types of Access Modifier 

1. Private 
2. Default
3. Public
4. Protected

1. Private - within the same class
	Members with the private access specifier are the most restricted. 
	They can only be accessed from within the same class. 
	They are not visible to other classes, even if they are in the same package.

Priorites:
	1. If use private access modifier then we access within the class only and we can't use in same package or different package.
	2. we can use private access modifier in the method,object,globle variable and constructor.
	3. we can't use private access modifier in the class and local variable.
	4. if you use private access modifier in method we can use the method in the same class only And u can't use in same package or different package.
	5. if you use private access modifier in constructor we can use the constructor in the same class only And u can't use in same package or different package.
	6. if you use private access modifier in global variable we can use the global variable in the same class only And u can't use in same package or different package.
	7. if you can't use private access modifier in the local varaible because the scope of local variable is method/block/constructor so there no sence for acesses modifier.
	8. if you can't use private access modifier in class because if you use private access modifier in class the class is not useable.

Important Notes 

A private constructor in Java is used in restricting object creation. 
If a constructor is declared as private, then its objects are only accessible from within the declared class. 
You cannot access its objects from outside the constructor class.   

2.Deafult-within the same Package 

 When no access specifier is specified, the member has package-level access. 
Members with package-level access can be accessed only within the same package. 
They are not accessible from classes in other packages.



Priorites:
	1. If use default access modifier then we access within the class and within the same package only and we can't use in different package.
	2. we can use Default access modifier in the class,method,object,globle variable and constructor.
	3. if you use Default access modifier in class we can use the class in the same class and and within the same package only And u can't use in  different package.
	4. if you use Default access modifier in method we can use the method in the same class and and within the same package only And u can't use in  different package.
	5. if you use private access modifier in constructor we can use the constructor in the same class and within the same package only And u can't use in  different package.
	6. if you use private access modifier in global variable we can use the global variable in the same class and within the same package only And u can't use in different package.
	7. if you can't use default access modifier in the local varaible because the scope of local variable is method/block/constructor so there no sence for acesses modifier.
	

