package com.indianBank.loan; // Creating director or folder using package

public class InterestCalculationUtility
{

  
	public static void main(String[] args)
	{
  
  		InterestCalculation ic = new InterestCalculation(); // object creating using class of other file for code reuseablity.
		
  		ic.calculate(); // Method Calling
  		
		System.out.println(ic.interest); // Print instance variable using object
  		
		
		System.out.println(InterestCalculation.name); // Print Static Variable using class name
	}

}