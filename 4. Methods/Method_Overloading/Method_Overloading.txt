								Polymorphisms
								
Polymorphism (ManyForms) - One interface Many Methods

Types Of Polymorphisms

1.Compile-Time Polymorphism (Method Overloading)
2.Runtime Polymorphism (Dynamic Polymorphism):

1. Method Overloading
		Same Method Name with different number of aruguments or with different
types of arguments.

Example

public class Method_Overloading
{
	public static void main(String [] args)
	{
		Method_Overloading object = new Method_Overloading(); // Creating Object
		
		// int result = object.add(23,24); // Call Method With two arguments
		
		System.out.println(object.add(23,24)); // Print the retrun Value from the method by direct calling
		
		float result2 = object.add(23.45F,24.34F); // Call Method With two arguments
		
		System.out.println(result2); // Print the retrun Value from the method 
		
		int result3 = object.add(23,34,56); // Call Method with three arguments
		
		System.out.println(result3); // Print the retrun Value from the method 
		
		object.add("Karkuvel","Prabakaran"); // Call Method With two String argument for concatination
	}
	public int add(int no1,int no2) // Method Defining With two parameters 
	{
		return no1 + no2 ;	// return only value
	}
	public float add(float no1,float no2) // Method Defining With two parameters same name but Different return type
	{
		return no1 + no2 ;	// return only value
	}
	public int add( int no1 , int no2 , int no3) // Method Defining with three parameters same name but different number of parameter 
	{
		return no1 + no2 + no3; // return only value
	}
	public void add(String name , String name1) // Method Defining with two parameters same name but different type return type and parameters
	{
		System.out.println(name + " "+name1);// Print the arguments
	}
	
}

output
47
47.79
113
Karkuvel Prabakaran



		