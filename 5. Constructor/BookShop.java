public class BookShop 
{
	String name; // Declare instance variable or non - static variable
	int pages; // Declare instance variable or non - static variable
	String author; // Declare instance variable or non - static variable
	
	// constructor
	// 1) Should have same class name 
	// 2) No return data type required
	// 3) call automatically when object is created 
	// 4) used to intializing object specific values
	
	// this keyword
	// this keyword refer to current object
	// this keyword cannot be used in static
	
	public BookShop(String name, int pages , String author) // constructor
	{
		this.name = name;       // this keyword is refered as current object name 
		this.pages = pages;
		this.author = author;
		
	}
	public static void main(String [] args )
	{
		BookShop book1 = new BookShop("XYZ",2000,"Author 1"); // object creating and initilize the object value using Constructor
		
		//book1.name = "abc";
		//book1.pages = 150;
		//book1.author = "auther1";
		
		BookShop book2 = new BookShop("ABC",200,"Author 2"); // object creating and initilize the object value using Constructor
		
		//book2.name = "xyz";
		//book2.pages = 150;
		//book2.author = "auther2";
		
		book1.buy(); // call method using object
		book2.buy(); // call method using object
		
		
	}
	public void buy() // Method defining
	{
		System.out.println("Book Name: "+name+" \nPages: "+pages+" \nAuthor Name: "+author); // printing the values
		
	}
}

/* 
output
Book Name: XYZ 
Pages: 2000 
Author Name: Author 1
Book Name: ABC 
Pages: 200 
Author Name: Author 2
*/