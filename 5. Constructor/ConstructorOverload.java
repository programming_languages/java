public class ConstructorOverload
{
	/*
		Constructor Overloading - the Constructor with same name with different type of aruguments or
		different number of arguments
	*/
	
	String name; // declare instance variable
	int age; // declare instance variable
	
	public ConstructorOverload(String name , int age)
	{
		this.name = name; //this keyword refers to current object
		this.age = age; //this keyword refers to current object
		
	
	}
	
	public ConstructorOverload(String name )
	{
		this.name = name; //this keyword refers to current object
	}
	
	public static void main(String [] args)
	{
		//Object creating with parameters and it call constructor to assign value to object
		ConstructorOverload object = new ConstructorOverload("Praba",22); 
		
		//Object creating with parameters and it call constructor to assign value to object
		ConstructorOverload object1 = new ConstructorOverload("karkuvel");
		
		System.out.println("Name: "+object.name+"\nAge: " + object.age+" =====> Object Output");//Printing statement
		
		System.out.println("Name: "+object1.name+" =====> Object 1 Output");//Printing Statement
		
	}
}