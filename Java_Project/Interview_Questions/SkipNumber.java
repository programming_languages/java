package Interview_Questions;

class SkipNumber 
{

    public static void main(String[] args) 
    {
        String word = "12awsr554oidvk";
        removeNumber(word);
    }
    public static void removeNumber(String word)
    {
        String ans = "";
        for(int i =0;i<word.length();i++)
        {
            if(!(Character.isDigit(word.charAt(i))))
            {
                ans = ans + word.charAt(i);
            }
        }
        System.out.println("Answer: "+ans);
    }

}