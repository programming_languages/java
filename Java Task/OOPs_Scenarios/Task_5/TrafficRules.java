package tamilnadu.chennai;

public interface TrafficRules
{
	String trafficCommissioner = "Kavin"; // default static and final variable 
	
	void goByDieselVehicle(); // default abstract method 
	
	void goByBicycle(); // default abstract method
}