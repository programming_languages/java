public class ActorSivakumar implements Actor
{
	//Static variable 
	static String address = "Coimbator";
	
	//instance Variable
	int age;
	String carName;
	
	public ActorSivakumar(int age, String carName) // Parameter constructor overloaded
	{
		//this refers to current object
		this.age = age; 
		this.carName = carName;
	}
	
	public ActorSivakumar() // No-args Constructor
	{
	
	}
	
	public static void main(String [] args)
	{
		ActorSivakumar as = new ActorSivakumar(65,"Audi Car"); // object creating with arguments
		
		//Calling Method
		as.act();
		as.dance();
		as.sing();
		as.speaking();
		
		// Access static variable
		System.out.println(as.address);
		
		
		Actor ac = new ActorSivakumar(); // Dynamic Binding 
		
		//Calling Method
		ac.act();
		ac.dance();
		ac.sing();
		// ac.speaking(); it shows error because the methon not in interface 
		
		//Access Static variable
		System.out.println(ac.address);
		
		// ac.address it shows output "Chennai" because in interface the variable is default static and final so it print the value "chennai"  
		
		
	}
	
	public void act() // Implement the method from interface
	{
		System.out.println("Acting");
	}
	
	public void dance() // Implement the method from interface
	{
		System.out.println("Dancing");
	}
	
	public void sing() // Implement the method from interface
	{
		System.out.println("Singing");
	}
	
	public void speaking() // Method Defining
	{
		System.out.println("Speaking");
	}
	
}