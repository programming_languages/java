public class ReverseNumber
{
	public static void main(String [] args)
	{
		ReverseNumber rn = new ReverseNumber();
		System.out.println(rn.reverse(1534236469));
	}
	public int reverse(int x) {
        long reverse = 0;
       	if(x > 0) 
		{
		   while(x > 0)
				{
						int remainder = x % 10;
						reverse = reverse * 10 + remainder;
						x = x / 10; 
				}
		}
		if(x < 0) 
		{
		   x = Math.abs(x);	
		   while(x > 0)
		   {
				int remainder = x % 10;
				reverse = reverse * 10 + remainder;
				x = x / 10; 
		   }
		   reverse = reverse * -1;
		}
        return reverse;

     }
}