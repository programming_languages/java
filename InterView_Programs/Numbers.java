import  java.util.Scanner;

public class Numbers
{
	public static void main(String [] args)
	{
		Numbers n = new Numbers();
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a Number");
		int no = sc.nextInt();
		//int result = n.sumDigits(1234);
		//int result = n.sumDigitsAdvance(23456373);
		//int result = n.countDigits(23456373);
		//int result = n.reverseNumber(1234);
		n.armstrongNumber(no);
		//n.isPalindrome(121);
		//n.splitNumbersReverse(1234);
		
	}
	
	public void armstrongNumber(int no)
	{
		int armstrongnumber = 0;
		int temp = no;
		while(no > 0)
		{
			int remainder = no % 10;
			armstrongnumber = armstrongnumber + findPower(remainder,countDigits(temp));
			no = no / 10;
			
		}
		if(armstrongnumber == temp)
		{
			System.out.println("The Number is ArmstrongNumber");
		}
		else
		{
			System.out.println("The Number is Not ArmstrongNumber");
		}
		
	}
	
	public int findPower(int base, int power)
	{
		int result = 1;
		
		while(power > 0)
		{
			result = result * base;
			power--; 
		}
		return result;
	}
	
	public void isPalindrome(int no)
	{
		int result = reverseNumber(Math.abs(no));
		if(result == Math.abs(no))
		{
			System.out.println("The Number is Palindrome");
		}
		else
		{
			System.out.println("The Number is Not Palindrome");
		}
		
	}
	public int reverseNumber(int no)
	{
		int reverse = 0;
		while(no > 0)
		{
			int remainder = no % 10;
			reverse  = (reverse * 10) + remainder;
			no = no / 10;
		}
		return reverse;
	}
	public int countDigits(int no)
	{
		int count = 0;
		while (no > 0)
		{
			int remainder = no % 10;
			no = no / 10;
			count++;
		}
		return count;
	}
	
	//This Method is used add the digits till in single digit number
	public int sumDigitsAdvance(int no)
	{
		int result = sumDigits(no);
		while(result > 9)
		{
			result = sumDigits(result);	
		}
		return result;
	}
	
	public int sumDigits(int no)
	{
		int sum = 0;
		while(no > 0)
		{
			int remainder = no % 10;
			sum = sum + remainder;
			no = no / 10;
		}
		return sum;
	}
	
	public void splitNumbersReverse(int no)
	{
		while(no > 0)
		{
			int remainder = no % 10;
			System.out.println(remainder);
			no = no / 10; 
		}
	}
}