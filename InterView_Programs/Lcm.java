import java.util.Scanner;
import java.util.InputMismatchException;


public class Lcm
{
	public static void main(String [] args)
	{
		//LCM - Least Comman Multiples
		
		Lcm lcm = new Lcm();
		lcm.findLCM();
		
	}
	public void findLCM()
	{
		int no = 1;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Three Numbers For Finding LCM: ");
		try
		{
			int no1 = sc.nextInt();
			int no2 = sc.nextInt();
			int no3 = sc.nextInt();
			while(true)
			{
				if(no/no1 == 0 && no/no2 == 0 && no/no3 == 0 )
				{
					System.out.println("The LCM is "+ no);
					break;
				}
				no++;
			}
		}
		catch(ArithmeticException ae)
		{
			System.out.println("Zero are not allowed");
			findLCM();
		}
		catch(InputMismatchException ie)
		{
			System.out.println("Your values are not vaild please check");
			findLCM();
		}
		catch(Exception e)
		{
			System.out.println("SomeThing Went Wrong");
			findLCM();
		}
		
		
	}
}