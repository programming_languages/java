public class Power
{
	public static void main(String [] args)
	{
		Power p = new Power();
		int no = 1;
		while(no <= 5)
		{
			//p.findPower(no,no); //1^1 2^2 3^3 4^4 5^5
			//p.findPower(no,no-1); //1^0 2^1 3^2 4^3 5^4 
			//p.findPower(no,no+1); //1^2 2^3 3^4 4^5 5^6
			//p.findPower(no,2); //1^2 2^2 3^2 4^2 5^2 
			//p.findPower(no,3); //1^3 2^3 3^3 4^3 5^3 
			no++;
		}
	}
	public void findPower(int base, int power)
	{
		int result = 1;
		while(power > 0)
		{
			result = result * base;
			power --;
		}
		System.out.print(result+" ");
	}
}