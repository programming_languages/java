public class StateGovt extends CentralGovt // Extends to CentralGovt class
{
	final int price = 100; // Final Keyword use to constant the price
  
	public static void main(String[] args)
	{

  		//CentralGovt sg = new StateGovt(); // Object Creating for superclass and create memory in subclass(Dynamic Binding)
  		//sg.rs2000(); // Methood Calling
  		//sg.rice20kg(); // Method calling
  
	}
	public void rs2000() // Method Defining
	{
  		System.out.println(price);
  		System.out.println("rs2000");
	}
}