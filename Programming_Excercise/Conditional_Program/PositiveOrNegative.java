import java.util.Scanner;

// Task - Write a Java program to get a number from the user and print whether it is positive or negative. 

public class PositiveOrNegative
{
	public static void main(String [] args)
	{
		
		PositiveOrNegative pn = new PositiveOrNegative();
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter the Number: ");
		
		double no = sc.nextDouble();
		
		pn.checkPositiveOrNegative(no);
			
	}
	public void checkPositiveOrNegative(double no)
	{
		if(no < 0)
		{
			System.out.println("The Number is Neagtive");
		}
		else if(no == 0)
		{
			System.out.println("The Number is Zero");
		}
		else
		{
			System.out.println("The Number is Positive");
		}
	}
	
	
}