package ExceptionHandling;

import java.util.Scanner;

public class ExceptionHandlingDemo 
{

	public static void main(String[] args) throws Exception 
	{
		try
		{
			System.out.println("Dividing Number :"+1/0);
		}
		catch(Exception e)
		{
			System.out.println("The Program is stoped during the division of 0");
		}
		finally
		{
			System.out.println("Exeuison  is completed");
		}
	}

}
