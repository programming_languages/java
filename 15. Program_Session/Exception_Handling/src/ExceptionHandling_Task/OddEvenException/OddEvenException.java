package ExceptionHandling_Task.OddEvenException;

public class OddEvenException 
{
	public void oddEvenException(int no) 
	{
		if(no%2 != 0)
		{
			throw new ArithmeticException(no + " is odd.");
		}
		else
		{
			System.out.println("It is a Even Number");
		}
	}
}
