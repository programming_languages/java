package BankSoftware;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Scanner;

public class Branch 
{
	ArrayList<Customer> bankCustomer = new ArrayList<>();
	Scanner scanner = new Scanner(System.in);
	private String name;
	private double transaction;
	
	public void addCustomer()
	{
		System.out.println("Add the customer's name");
		name = scanner.nextLine();	
		System.out.println("Add the customer's initial transaction");
		transaction = scanner.nextDouble();
		Customer customer = new Customer(name, transaction);
		System.out.println("Customer's name is " + customer.getName());
        System.out.println("Customer made the transaction of  " + customer.getTransaction());
        bankCustomer.add(customer);
	}
	public void addTransaction()
	{
		System.out.println("Which customer would you like to add transaction to?");
		name = scanner.nextLine();
		for(int i = 0;i < bankCustomer.size();i++)
		{
			if(bankCustomer.get(i).getName().equals(name))
			{
				 System.out.println("Customer found");
				 int index = i;
				 System.out.println("How much more would you like to add?");
				 transaction = scanner.nextDouble();
				 double counter = bankCustomer.get(index).getTransaction() + transaction;
				 bankCustomer.get(i).setTransaction(counter);
				 System.out.println("Transaction completed. Your account now has $" + counter);
				 break;
			}
			else if(bankCustomer.size() == 0)
			{
				System.out.println("There are no customers");
			}
			else
			{
				System.out.println("Customer not found");
			}
		}
	}
	public void printSummery()
	{
		if(bankCustomer.size() == 0)
		{
			System.out.println("There are no customers");
		}
		else
		{
			 for (int i = 0; i < bankCustomer.size(); i++) 
			 {
	             System.out.println(bankCustomer.get(i).getTransaction());
	             System.out.println(i + 1 + ". " + bankCustomer.get(i).getName() +
	                        " $" + bankCustomer.get(i).getTransaction());
			 }
		}
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getTransaction() {
		return transaction;
	}
	public void setTransaction(double transaction) {
		this.transaction = transaction;
	}
	
	
}
