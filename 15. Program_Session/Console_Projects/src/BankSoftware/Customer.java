package BankSoftware;

public class Customer 
{
	private String name;
	private double transaction;
	
	public Customer(String name,double transaction)
	{
		this.name = name;
		this.transaction = transaction;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getTransaction() {
		return transaction;
	}

	public void setTransaction(double transaction) {
		this.transaction = transaction;
	}
	
	
}
