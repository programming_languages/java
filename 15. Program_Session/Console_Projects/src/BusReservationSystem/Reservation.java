package BusReservationSystem;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

public class Reservation 
{

	public void App()
	{
		TicketBooking tb = new TicketBooking();
		while(true)
		{
			System.out.println("Welcome to Bus Reservation System");
			System.out.println("1. Book Tickets ");
			System.out.println("2. Cancel Tickets");
			System.out.println("3. Print Statement");
			System.out.println("4. Check Tickets Availability");
			System.out.println("5. Exit");
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter Your Choice:");
			try
			{
				int choice = sc.nextInt();
				switch(choice)
				{
					case 1:
						System.out.println();
						try
						{
							tb.bookingTickets();
						}
						catch(Exception e)
						{
							System.out.println("Connection failed ....");
						}
						System.out.println();
						break;
					case 2:
						System.out.println();
						try
						{
							tb.cancelTicket();
						}
						catch(Exception e)
						{
							System.out.println("Connection failed ....");
						}
						System.out.println();
						break;
					case 3:
						System.out.println();
						try
						{
							tb.printStatement();
						}
						catch(Exception e)
						{
							System.out.println("Connection failed ....");
						}
						System.out.println();
						break;
					case 4:
						System.out.println();
						try
						{
							tb.checkAvailability();
						}
						catch(Exception e)
						{
							System.out.println("Connection failed ....");
						}
						System.out.println();
						break;
					case 5:
						System.out.println("Exiting the application. Thank you!");
						System.exit(0);
						break;
					default:
						System.out.println("Invalid choice. Please enter a number between 1 and 5.");
						
				}

			}
			catch(InputMismatchException e)
			{
				System.out.println("Please Enter Right Key....");
				System.out.println();
			}
			catch(Exception e)
			{
				System.out.println("Something Went Wrong");
			}
		}

	}

}
