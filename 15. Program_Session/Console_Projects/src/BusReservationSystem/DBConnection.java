package BusReservationSystem;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection 
{
	private static String url = "jdbc:postgresql://localhost:5432/busreservation";
	private static String userName = "postgres";
	private static String password = "5321";
	public static Connection getConnection() throws SQLException
	{
		return DriverManager.getConnection(url,userName,password);
		
	}

}
