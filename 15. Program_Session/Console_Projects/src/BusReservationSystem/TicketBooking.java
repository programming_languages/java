package BusReservationSystem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class TicketBooking 
{

	public void checkAvailability() throws SQLException
	{
		Connection con = DBConnection.getConnection();
		Statement st = con.createStatement();
		String query = "select * from bus";
		ResultSet rs = st.executeQuery(query);
		while(rs.next())
		{
			System.out.println("The Bus no :"+rs.getInt(1));
			System.out.println("Board From: "+ rs.getString(2).toUpperCase());
			System.out.println("Destination: "+ rs.getString(3).toUpperCase());
			System.out.println("The Bus is (AC/NON-AC): "+rs.getString(4).toUpperCase());
			System.out.println("Available Tickets are  "+rs.getInt(5));
			System.out.println();
		}
		
	}
	
	public void displayBus() throws SQLException
	{
		Connection con = DBConnection.getConnection();
		Statement st = con.createStatement();
		String query = "select * from bus";
		ResultSet rs = st.executeQuery(query);
		while(rs.next())
		{
			System.out.println("The Bus no :"+rs.getInt(1));
			System.out.println("Board From: "+ rs.getString(2).toUpperCase());
			System.out.println("Destination: "+ rs.getString(3).toUpperCase());
			System.out.println("The Bus is (AC/NON-AC): "+rs.getString(4).toUpperCase());
			System.out.println();
		}
	}

	public void bookingTickets() throws SQLException 
	{
		System.out.println("...........Buses............. ");
		System.out.println();
		this.displayBus();
		Random random = new Random();
		Scanner sc = new Scanner(System.in);
		int id = random.nextInt(100);
		System.out.println("How many Tickets want to book: ");
		int no = sc.nextInt();
		System.out.println("Bus No: ");
		int bus_no = sc.nextInt();
		String query = "Select capacity from bus where bus_no ="+bus_no;
		Connection con = DBConnection.getConnection();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
		rs.next();
		int seat = rs.getInt(1)-no;
		
		if(no < rs.getInt(1))
		{
			while (no > 0)
			{
				id++;
				System.out.println("Enter your Name: ");
				String name = sc.next();
				System.out.println("Enter your Age: ");
				int age = sc.nextInt();
				System.out.println("Enter your Gender: ");
				String gender = sc.next();
				System.out.println("Enter your Mobile No: ");
				long mno = sc.nextLong();
				System.out.println("Enter your Email: ");
				String email = sc.next();
				System.out.println("Enter your Boarding Place: ");
				String boarding_place = sc.next();
				System.out.println("Enter your Destination Place: ");
				String destination_place = sc.next();
				System.out.println("The ID is "+ id );
				System.out.println("And use this ID to Print Statement or Cancel Ticket");
				System.out.println("Sucessfully Ticket Booked...................... ");
				no--;
				String query3 = "insert into passenger values (?,?,?,?,?,?,?,?,?)";
				PreparedStatement pst = con.prepareStatement(query3);
				pst.setInt(1, id);
				pst.setString(2,name);
				pst.setInt(3,age);
				pst.setString(4,gender);
				pst.setLong(5,mno);
				pst.setString(6, email);
				pst.setInt(7,bus_no);
				pst.setString(8, boarding_place);
				pst.setString(9, destination_place);
				pst.executeUpdate();
				pst.close();
				
			}
		
		}
		else
		{
			System.out.println("Sorry For Inconvinence");
			System.out.println("The Available Tickets are "+seat);
		}
		rs.close();
		String query1 = "update bus set capacity = "+seat+" Where bus_no = "+bus_no;
		st.executeUpdate(query1);
		
	}
	public void printStatement() throws SQLException
	{
		try
		{
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter your ID: ");
			int id = sc.nextInt();
			Connection con = DBConnection.getConnection();
			Statement st = con.createStatement();
			String query = "select * from passenger where id = "+id;
			ResultSet rs = st.executeQuery(query);
			while(rs.next())
			{
				System.out.println("Name: "+rs.getString(2));
				System.out.println("Age: "+rs.getInt(3));
				System.out.println("Gender: "+rs.getString(4));
				System.out.println("Mobile No: "+rs.getLong(5));
				System.out.println("Email: "+rs.getString(6));
				System.out.println("Bus No: "+rs.getInt(7));
				System.out.println("Boarding Place: "+rs.getString(8));
				System.out.println("Destination Place: "+rs.getString(9));
				
			}
		}
		catch(InputMismatchException ie)
		{
			System.out.println("Please Verify the ID");
			printStatement();
		}
		catch(Exception e)
		{
			System.out.println("Enter Valid ID");
		}
	}
	public void cancelTicket() throws SQLException
	{
		Connection con = DBConnection.getConnection();
		Statement st = con.createStatement();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Your Id For Cancellation: ");
		int id = sc.nextInt();
		String query2 = "select bus_no from passenger where id ="+id;
		ResultSet rs = st.executeQuery(query2);
		rs.next();
		int bus_no = rs.getInt(1);
		rs.close();
		String query3 = "select capacity from bus where bus_no = "+bus_no;
		ResultSet rs1 = st.executeQuery(query3);
		rs1.next();
		int capacity = rs1.getInt(1);
		rs1.close();
		String query4 = "update bus set capacity = "+(capacity+1)+" where bus_no = "+bus_no;
		st.executeUpdate(query4);
		String query = "delete from passenger where id = "+id;
		st.executeUpdate(query);
		System.out.println("SucessFully Cancelled.........");
		
	}
}
