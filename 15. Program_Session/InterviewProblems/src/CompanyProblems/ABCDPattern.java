package CompanyProblems;

import java.util.Scanner;

public class ABCDPattern 
{

	public static void main(String[] args) 
	{
		patternPrinting();
	}
	public static void patternPrinting()
	{
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Number");
		int no = scanner.nextInt();
		char ch = 'A';
		int start = ch;
		int temp =0;
		for(int i =0;i<no;i++)
		{
			int n =0;
			for(int j = i;j<no;j++)
			{
				System.out.print((char)(start+n));
				temp = start+n;
				n++;
			}
			n =0;
			for(int k = i;k<no;k++)
			{
				System.out.print((char)(temp-n));
				n++;
			}
			System.out.println();
		}
		
	}

}
