package CompanyProblems;

import java.util.Arrays;
import java.util.Scanner;

public class CharacterSum 
{
    public static void main(String[] args) 
    {
        sumCharacter();
    }
    public static void sumCharacter()
    {
        char[] alphabet = new char[26];
        int start = 'A';
        for(int i =0;i<26;i++)
        {
            alphabet[i] = (char)(start+i);
        }
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a word");
        String word = scanner.nextLine().toUpperCase();
        char[] ch = word.toCharArray();
        int sum =0;
        for(int i =0;i<ch.length;i++)
        {
            for(int j=0;j<alphabet.length;j++)
            {
                if(ch[i] == alphabet[j])
                {
                    sum = sum + j + 1;
                }
            }
        }
        System.out.println(sum);
    }
}
