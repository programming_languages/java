package learn_jdbc_odbc;

import java.sql.*;
import java.util.Scanner;

public class JDBCDemo 
{

	public static void main(String[] args) 
	{
//		readRecordsDB();
//		inserRecordDB();
//		inserRecordPstDB();
		deleteRecordDB();
		
	}
	//Read Data From DataBase Table
	public static void readRecordsDB()
	{
		String url = "jdbc:postgresql://localhost:5432/jdbcdemo";
		String userName = "postgres";
		String password = "5321";
		System.out.println("Enter your ID");
		Scanner sc = new Scanner(System.in);
		int id = sc.nextInt();
		String query = "select * from student_marklist where id ="+id;
		try 
		{
			Connection con = DriverManager.getConnection(url,userName,password);
			Statement st = con.createStatement();
			ResultSet rs =st.executeQuery(query);
			while(rs.next())
			{
				System.out.println("ID is "+rs.getInt(1));
				System.out.println("Name is "+rs.getString(2));
				System.out.println("GPA is "+rs.getFloat(3));
			}
			con.close();
			
		}
		catch(Exception e)
		{
			System.out.println("Connection Failed.......");
		}
	}
	//Insert Records To the Table
	public static void inserRecordDB()
	{
		String url = "jdbc:postgresql://localhost:5432/jdbcdemo";
		String userName = "postgres";
		String password = "5321";
		String query = "insert into student_marklist values(5,'Karkuvel Prabakaran',9.5)";
		
		try
		{
			Connection con = DriverManager.getConnection(url, userName, password);
			Statement st = con.createStatement();
			int rows = st.executeUpdate(query);
			System.out.println("The Number of rows affected: "+rows);
			
		}
		catch(Exception e)
		{
			System.out.println("Connection Failed........");
		}
		
	}
	
	//Insert Record From user using prepared statement 
	public static void inserRecordPstDB()
	{
		String url = "jdbc:postgresql://localhost:5432/jdbcdemo";
		String userName = "postgres";
		String password = "5321";
		Scanner sc  = new Scanner(System.in);
		System.out.println("Enter your ID: ");
		int id = sc.nextInt();
		System.out.print("Enter your Name: ");
		String name = sc.next();
		System.out.println("Enter your GPA: ");
		int gpa = sc.nextInt();
		String query ="insert into student_marklist values(?,?,?)";
		try
		{
			Connection con = DriverManager.getConnection(url, userName, password);
			PreparedStatement pst = con.prepareStatement(query);
			pst.setInt(1, id);
			pst.setString(2, name);
			pst.setInt(3, gpa);
			int rows = pst.executeUpdate();
			System.out.println("The Number of rows affected: "+rows);
		}
		catch(Exception e)
		{
			System.out.println("Connection Failed.........");
		}
	}
	//Delete From DB
	public static void deleteRecordDB()
	{
		String url = "jdbc:postgresql://localhost:5432/jdbcdemo";
		String userName = "postgres";
		String password = "5321";
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Id to delete: ");
		int id = sc.nextInt();
		String query = "delete from student_marklist where id = "+id;
		
		try
		{
			Connection con = DriverManager.getConnection(url,userName,password);
			Statement st = con.createStatement();
			int rows = st.executeUpdate(query);
			System.out.println("The Number of rows affected: "+rows);
		}
		catch(Exception e)
		{
			System.out.println("Connection Failed.........");
		}
	}

}
