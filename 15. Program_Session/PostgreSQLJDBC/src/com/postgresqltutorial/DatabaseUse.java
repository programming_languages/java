package com.postgresqltutorial;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class DatabaseUse {
	private final static String url = "jdbc:postgresql://localhost:5432/praba";
    private final static String user = "postgres";
    private final static String password = "5321";

	public static void main(String[] args) throws Exception {
		
		DatabaseUse db = new DatabaseUse();
		db.insertRecord();
	}
	public static void insertRecord() throws Exception{
		String query = "insert into students values ('john cena',22)";

		
		Connection con = DriverManager.getConnection(url,user,password);
		Statement st = con.createStatement();
		int rows = st.executeUpdate(query);
		
		System.out.println("Number of rows affected: " + rows);		
		con.close();
	}

}
