package com.form_Project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.form_Project.entity.FormEntity;
import com.form_Project.respository.FormRespository;

@Controller
public class FormController {
	
	@Autowired
	FormRespository repo;

	@GetMapping("/index")
	public ModelAndView addForm(FormEntity en)
	{
		ModelAndView mav = new ModelAndView("form");
		mav.addObject("entity",en);
		return mav;
	}
	
	@PostMapping("/index")
	public String saveForm(FormEntity en)
	{
		repo.save(en);
		return"refirect:/index";
	}

}
