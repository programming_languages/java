package com.form_Project.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.form_Project.entity.FormEntity;

public interface FormRespository extends JpaRepository<FormEntity, Long>
{

}
