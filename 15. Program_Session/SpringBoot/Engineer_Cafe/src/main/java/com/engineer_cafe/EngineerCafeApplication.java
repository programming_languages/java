package com.engineer_cafe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EngineerCafeApplication {

	public static void main(String[] args) {
		SpringApplication.run(EngineerCafeApplication.class, args);
		

	}

}
