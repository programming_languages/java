package com.engineer_cafe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.engineer_cafe.entity.Engineer_CafeEntity;
import com.engineer_cafe.repository.Engineer_CafeRepository;

@Controller
public class Engineer_CafeController 
{
	@Autowired
	public Engineer_CafeRepository repo; 
	
//	@RequestMapping("index")
//	public String engineerCafe()
//	{
//		
//		return "index";
//	}
	
	@GetMapping("/index")
	public ModelAndView bookTable(Engineer_CafeEntity eng) {
		System.out.println("hi");
	    ModelAndView mav = new ModelAndView("/index");
	    mav.addObject("book", eng);
	    return mav;
	}

	@PostMapping("/index")
	public String saveTable(Engineer_CafeEntity eng) {
	    repo.save(eng);
	    return "redirect:/index#book";
	}
	
	
	
}
