package com.engineer_cafe.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.engineer_cafe.entity.Engineer_CafeEntity;



public interface Engineer_CafeRepository extends JpaRepository<Engineer_CafeEntity, Long>
{
	

}