package com.engineer_cafe.entity;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "Book_Table")
public class Engineer_CafeEntity 
{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private String name;
	private String email;
	private long mno;
	private String location;
	@DateTimeFormat (pattern = "yyyy-mm-dd")
	private LocalDate date;
	private long no_person;
	private String seasion;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getMno() {
		return mno;
	}
	public void setMno(long mno) {
		this.mno = mno;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public long getNo_person() {
		return no_person;
	}
	public void setNo_person(long no_person) {
		this.no_person = no_person;
	}
	public String getSeasion() {
		return seasion;
	}
	public void setSeasion(String seasion) {
		this.seasion = seasion;
	}
	
	
}
