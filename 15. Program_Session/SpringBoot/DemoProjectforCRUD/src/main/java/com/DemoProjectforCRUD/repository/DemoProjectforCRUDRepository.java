package com.DemoProjectforCRUD.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.DemoProjectforCRUD.entity.DemoProjectforCRUDEntity;

@Repository
public interface DemoProjectforCRUDRepository extends JpaRepository<DemoProjectforCRUDEntity, Long>
{
	
}
