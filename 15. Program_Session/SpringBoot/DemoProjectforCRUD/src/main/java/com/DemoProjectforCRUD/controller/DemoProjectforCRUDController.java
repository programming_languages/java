package com.DemoProjectforCRUD.controller;

import java.util.ArrayList;
import java.util.ListIterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.DemoProjectforCRUD.entity.DemoProjectforCRUDEntity;
import com.DemoProjectforCRUD.repository.DemoProjectforCRUDRepository;

@Controller
public class DemoProjectforCRUDController 
{
	@Autowired
	public DemoProjectforCRUDRepository repo;
	
	static ArrayList<Object> userDetails = new ArrayList<>();
	
	@GetMapping("/index")
	public ModelAndView home()
	{
		ModelAndView mav = new ModelAndView("index");
		mav.addObject("usr",repo.findAll());
		return mav;
	}
	
	@GetMapping("/useradd")
	public ModelAndView userAdd(DemoProjectforCRUDEntity usr)
	{
		ModelAndView mav = new ModelAndView("useradd");
		mav.addObject("user",usr);
//		System.out.println(usr.getAge());
		return mav;
	}
	@PostMapping("/index")
	public String addUser(DemoProjectforCRUDEntity usr)
	{
//		System.out.println(usr.getAge());
		
		userDetails.add(usr);
			System.out.println(usr.getAge());
			System.out.println(usr.getEmail());
			System.out.println(usr.getId());
			System.out.println(usr.getName());
		repo.save(usr);
		return "redirect:/index";
	}
	
	@GetMapping("/updatedetails/{id}")
	public ModelAndView updateDetails(@PathVariable long id)
	{
		ModelAndView mav = new ModelAndView("updatedetails");
		mav.addObject("userdet",repo.findById(id).get());
		return mav;
	}
	
	@PostMapping("/detailsubmit/{id}")
	public String detailsUpdated(@PathVariable long id,DemoProjectforCRUDEntity entity)
	{
		entity.setId(id);
		repo.save(entity);
		return "redirect:/index";
	}
	
	@GetMapping("/deletedetails/{id}")
	public String getMethodName(@PathVariable long id) 
	{
		repo.deleteById(id);
		return "redirect:/index";
	}
	
}
