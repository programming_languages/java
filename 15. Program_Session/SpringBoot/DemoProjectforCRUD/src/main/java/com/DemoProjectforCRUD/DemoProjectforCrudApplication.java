package com.DemoProjectforCRUD;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoProjectforCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoProjectforCrudApplication.class, args);
	}

}
