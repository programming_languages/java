package com.liberayManagementSystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.liberayManagementSystem.entity.LiberayManagementSystemEntity;
import com.liberayManagementSystem.repository.LiberayManagementSystemRepository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@Controller
public class LiberayManagementSystemController 
{
	@Autowired
	LiberayManagementSystemRepository repo;
	
	@GetMapping("/index")
	public ModelAndView index()
	{
		ModelAndView mav = new ModelAndView("index");
		mav.addObject("book",repo.findAll());
		return mav;
	}
	
	@GetMapping("/addbook")
	public ModelAndView addbook(LiberayManagementSystemEntity book)
	{
		ModelAndView mav = new ModelAndView("addbook");
		mav.addObject("books", book);
		return mav;
	}
	
	@PostMapping("/index")
	public String savebook(LiberayManagementSystemEntity book)
	{
		repo.save(book);
		return "redirect:/index";
	}
	
}
