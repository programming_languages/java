package com.liberayManagementSystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LiberayManagementSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(LiberayManagementSystemApplication.class, args);
	}

}
