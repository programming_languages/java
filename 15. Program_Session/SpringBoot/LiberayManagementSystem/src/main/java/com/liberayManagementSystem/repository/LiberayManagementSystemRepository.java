package com.liberayManagementSystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.liberayManagementSystem.entity.LiberayManagementSystemEntity;

public interface LiberayManagementSystemRepository extends JpaRepository<LiberayManagementSystemEntity, Long> 
{

}
