package com.SchoolManagement.KSchool.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.SchoolManagement.KSchool.entity.KSchoolStudentEntity;

public interface KSchoolStudentRespository extends JpaRepository<KSchoolStudentEntity,Long>
{

}
