package com.SchoolManagement.KSchool.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.SchoolManagement.KSchool.entity.KSchoolStudentEntity;
import com.SchoolManagement.KSchool.entity.KSchoolTeacherEntity;
import com.SchoolManagement.KSchool.respository.KSchoolStudentRespository;
import com.SchoolManagement.KSchool.respository.KSchoolTeacherRespository;




@Controller
public class KSchoolController 
{
	@Autowired
	KSchoolStudentRespository srepo;
	@Autowired
	KSchoolTeacherRespository trepo;
	
	@GetMapping("/index")
	public String index()
	{
		return "index";
	}
	@GetMapping("/studentform")
	public ModelAndView addStudent(KSchoolStudentEntity stud)
	{
		ModelAndView mav = new ModelAndView("studentform");
		mav.addObject("student",stud);
		return mav;
		
	}
	
	@PostMapping("/studentform")
	public String saveStudent(KSchoolStudentEntity stud)
	{
		srepo.save(stud);
		return"redirect:/index";
	}
	
	@GetMapping("/studentlist")
	public ModelAndView studentlist()
	{
		ModelAndView mav = new ModelAndView("studentlist");
		mav.addObject("student", srepo.findAll());
		return mav;
	}
	@GetMapping("/teacherform")
	public ModelAndView teacherform(KSchoolTeacherEntity teac)
	{
		ModelAndView mav = new ModelAndView("teacherform");
		mav.addObject("teacher",teac);
		return mav;
	}
	
	@PostMapping("/teacherform")
	public String saveTeacher(KSchoolTeacherEntity teac)
	{
		trepo.save(teac);
		return "redirect:/index";
	}
	@GetMapping("/teacherlist")
	public ModelAndView teacherlist()
	{
		ModelAndView mav = new ModelAndView("teacherlist");
		mav.addObject("teacher",trepo.findAll());
		return mav;
	}
	@GetMapping("student/edit/{id}")
	public ModelAndView updateStudent(@PathVariable long id)
	{
		ModelAndView mav = new ModelAndView("editstudentform");
		mav.addObject("updatestd", srepo.findById(id).get());
		return mav;
	}
	@PostMapping("/student/{id}")
	public String savestudent(@PathVariable long id, KSchoolStudentEntity entity)
	{
		System.out.println("============================");
		entity.setId(id);
		srepo.save(entity);
		return "redirect:/index";
	}
}
