package com.SchoolManagement.KSchool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KSchoolApplication {

	public static void main(String[] args) {
		SpringApplication.run(KSchoolApplication.class, args);
	}

}
