package com.SchoolManagement.KSchool.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.SchoolManagement.KSchool.entity.KSchoolTeacherEntity;

public interface KSchoolTeacherRespository extends JpaRepository<KSchoolTeacherEntity,Long>
{

}

