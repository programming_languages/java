package ProgramSolution;

public class MergerString 
{
    public static void main(String[] args) 
    {
        String word1 = "abcd", word2 = "pq";
        mergeAlternately(word1,word2);
    }
    public static String mergeAlternately(String word1, String word2) 
    {
        String output = "";
        char[] ch = word1.toCharArray();
        char[] ch1 = word2.toCharArray();
        for(int i = 0;i<ch.length;i++)
        {
            output += ch[i];
            if(i<ch1.length)
            {
                for(int j = i;j <= i;j++)
                {
                    output += ch1[i];
                }
            }
        }
        if(ch.length<ch1.length)
        {
            for(int i = ch.length;i<=ch1.length-1;i++)
            {
                output += ch1[i];
            }   
        }
        System.out.println(output);
        return output;   
    }
}
