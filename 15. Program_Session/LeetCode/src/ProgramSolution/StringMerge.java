package ProgramSolution;

public class StringMerge {

	public static void main(String[] args) 
    {
        String word1 = "ab", word2 = "pqrs";
        mergeAlternately(word1,word2);
    }
    public static String mergeAlternately(String word1, String word2) 
    {
        String output = "";
        for(int i = 0;i<word1.length();i++)
        {
            output += word1.charAt(i);
            for(int j = i;j<=i;j++)
            {
                output += word2.charAt(j);
            }
        }
        if(word1.length()<word2.length())
        {
            for(int i = word1.length();i<=word2.length()-1;i++)
            {
                output += word2.charAt(i);
            }   
        }
        System.out.println(output);
        return output;   
    }

}
