package ProgramSolution;

import java.util.ArrayList;


public class SortSentence 
{
    public static void main(String[] args) 
    {
        String s = "Myself2 Me1 I4 and3";
        System.out.println(sortSentence(s));
    }
    public static String sortSentence(String s)
    {
        String result = "";
        ArrayList<String> senList = new ArrayList<>();
        String temp = "";
        for(int i =0;i<s.length();i++)
        {
            if(s.charAt(i) == ' ')
            {
                senList.add(temp);
                temp = "";
            }
            else if(i == s.length()-1)
            {
                temp += s.charAt(i);
                senList.add(temp);
                temp ="";
            }
            else
            {
                temp += s.charAt(i);
            }
        }
        String[] order  = new String[senList.size()];
        int no =1;
        for(int i =0;i<senList.size();i++)
        {
           for(int j = 0;j<senList.size();j++)
           {
                if(senList.get(j).contains(String.valueOf(no)))
                {
                    order[no-1] = senList.get(j);
                    no++;
                }
           }
        }
        for(int i = 0;i<order.length;i++)
        {
            int n = order[i].length()-1;
            for(int j=0; j<n;j++)
            {
                 result += order[i].charAt(j);
            }
            if(i == order.length-1)
            {
                break;
            }
            result += " ";
        }
        return result;
    }
}
