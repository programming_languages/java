package ProgramSolution;

public class FindMaxPair 
{

    
     
    public static void main(String[] args, String string) 
    {
        FindMaxPair fmp = new FindMaxPair();
        String[] words = {"cd","ac","dc","ca"};
        System.out.println(fmp.maximumNumberOfStringPairs(words));

    }
    public int maximumNumberOfStringPairs(String[] words)
    {
        int count = 0;
        
        for(int i =0;i<words.length;i++)
        {

            for(int j =0;j<words.length;j++)
            {
                if(i != j)
                {
                    if(words[i].equals(reverseString(words[j])))
                    {
                        count++;
                    }
                }
            }
        } 
        return count;    
    }
    public String reverseString(String letter)
    {
        String str = "";
        for(int i = letter.length()-1;i>=0;i--)
        {
            str += letter.charAt(i);   
        }
        return str;
    }
}


