package ProgramSolution;

public class ReversePrefix 
{
    public static void main(String[] args) 
    {
        ReversePrefix rp = new ReversePrefix();
        String word = "abcdefd" ;
        char ch = 'd';
        rp.reversePrefix(word, ch);
    }
    public String reversePrefix(String word, char ch) 
    {
        int point = word.indexOf(ch);
        String split = "";
        for(int i = point;i>=0;i--)
        {
            split += word.charAt(i); 
        }
        for(int i = point;i<word.length();i++)
        {
            split += word.charAt(i);
        }
        System.out.println(split);
        return split;
    }
    public String reverseString(String word)
    {
        return "";
    }
}
