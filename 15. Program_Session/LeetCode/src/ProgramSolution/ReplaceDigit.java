package ProgramSolution;



public class ReplaceDigit 
{

	public static void main(String[] args) 
	{
		ReplaceDigit rd = new ReplaceDigit();
		rd.replaceDigits("a1b2c3d4e");
	}
	public String replaceDigits(String s) 
	{
		char[] ch = s.toCharArray();
		String str = "";
		for(int i = 1;i<ch.length;i += 2)
		{
			String st = String.valueOf(ch[i]);
			int no = Integer.parseInt(st);
			int temp= ch[i-1]+no;
			ch[i] = (char)temp;	
		}
		for(char c : ch)
		{
			str += c;
		}
		System.out.println(str);
		return str;
    }

}
