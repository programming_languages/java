package ProgramSolution;

public class RemoveTrailingZero 
{
    public static void main(String[] args) 
    {
        System.out.println(removeTrailingZeros("123"));
    }
    public static String removeTrailingZeros(String num) 
    {
        String output ="";
        int triger = 0;
        for(int i = num.length()-1;i>=0;i--)
        {
            if(num.charAt(i) == '0')
            { 
                if(num.charAt(i-1) != '0')
                {
                    triger = i;
                    break;
                }
            }
        }
        if(triger == 0)
        {
            return num;
        }
        for(int i = 0;i<triger;i++)
        {
            output += num.charAt(i);
        }
        System.out.println(output);
        return output;
    }
}
