package ProgramSolution;

import java.util.Arrays;

public class SeniorCitizen 
{
    public static void main(String[] args) 
    {
        String[] details = {"9751302862F0693","3888560693F7262","5485983835F0649","2580974299F6042","9976672161M6561","0234451011F8013","4294552179O6482"};
        countSeniors(details);
    }
    public static int countSeniors(String[] details) 
    {
        String[] arr = new String[details.length];
        int no = 0;
        for(int i =0;i<details.length;i++)
        {
           if(details[i].charAt(10) == 'F' || details[i].charAt(10) == 'M' )
           {
                arr[no] = "";
                arr[no] += details[i].charAt(11);
                arr[no] += details[i].charAt(12);
                no++;
           }
           else
           {
             int n = details[i].length();
             arr[no] = "";
             arr[no] += details[i].charAt(n-4);
             arr[no] += details[i].charAt(n-3);
             no++;
           }
        }
        System.out.println(Arrays.toString(arr));
        int count =0; 
        for(int i =0;i<arr.length;i++)
        {
            if(arr[i] != null)
            {
                if(Integer.parseInt(arr[i]) > 60)
                {
                    count++;
                }
            }
        }
        System.out.println(count);
        return count;
    }
}
