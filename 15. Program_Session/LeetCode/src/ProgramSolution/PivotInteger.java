package ProgramSolution;

public class PivotInteger 
{
	public static void main(String[] args) 
	{
		PivotInteger pi = new PivotInteger();
		System.out.println(pi.pivotInteger(4));
	}
	public int pivotInteger(int n) 
	{
		int leftSide = 0;
		int rightSide = 0;
		for(int i =1; i<=n; i++)
		{
			leftSide += i;
		}
		for(int i =1;i<=n;i++)
		{
			rightSide += i;
			if(rightSide == leftSide)
			{
				return i;
			}
			leftSide -= i;
		}
		return -1;
	}
}
