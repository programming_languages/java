package ProgramSolution;

import java.util.HashSet;

public class MinimizeStringLength 
{
    public static void main(String[] args) 
    {
        String s = "aaabc";
        minimizedStringLength(s);
    }
    public static int minimizedStringLength(String s) 
    {
        HashSet<Character> listCharacter = new HashSet<>();
        char[] ch = s.toCharArray();
        for(char c : ch)
        {
            listCharacter.add(c);
        }
        return listCharacter.size();
    }
}
