package ProgramSolution;

public class LargestSubString 
{
    public static void main(String[] args) 
    {
        String s = "bacdea";
        maxLengthBetweenEqualCharacters(s);
    }
    public static int maxLengthBetweenEqualCharacters(String s) 
    {
        int count = 0;
        char[] ch = s.toCharArray();
        int startState = 0;
        int endState = 0;
        boolean isEnter =  false;
        for(int i = 0;i<ch.length;i++)
        {
            for(int j = i+1;j<ch.length;j++)
            {
                if(ch[i] == ch[j])
                {
                    startState = i;
                    endState = j;
                    isEnter = true;
                    break;
                }
                
            }
            if(isEnter)
            {
                break;
            }
        }
        count = (endState - startState) -1;
        System.out.println(count);
        return count;
    }
}
