package Arrays.utilArrays;

import java.util.Arrays;

public class RefferenceVariable 
{

	public static void main(String[] args) 
	{
		int[] arr = new int[5];
		int[] arr1 = arr;
		arr1[0] = 12;
		modifyArray(arr1);
		System.out.println(Arrays.toString(arr)); //Output - [12, 22, 0, 0, 0]
		System.out.println(Arrays.toString(arr1)); //Output - [12, 22, 0, 0, 0]
		//The output are same because they pointing the same memory address it is applicable for only for array
	}
	public static void modifyArray(int[] arr)
	{
		arr[1] = 22;
	}

}
