package Arrays.utilArrays;

public class VarargsArgument 
{

	public static void main(String... args) 
	{
		System.out.println("Hi Karkuvel Prabakaran");
		String[] str = "Hi Karkuvel Prabakaran".split(" ");
		printText(str);
		System.out.println("_".repeat(20));
		printText("Hi","Karkuvel");
		printText("praba");	
		String[] str1 = {"Hello","Karkuvel","Prabakaran"};
		System.out.println(String.join(",", str1));
	}
	private static void printText(String... text)
	{
		for(String str : text)
		{
			System.out.println(str);
		}
	}

}
