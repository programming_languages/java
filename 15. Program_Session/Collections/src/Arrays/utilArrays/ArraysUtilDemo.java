package Arrays.utilArrays;

import java.util.Arrays;
import java.util.Random;

public class ArraysUtilDemo 
{

	public static void main(String[] args) 
	{
		int[] firstArray = getRandomArray(10);
		System.out.println(Arrays.toString(firstArray));
		Arrays.sort(firstArray,0,5);
		System.out.println(Arrays.toString(firstArray));
		
		int[] secondArray = new int[10];
		System.out.println(Arrays.toString(secondArray));
		Arrays.fill(secondArray, 5);
		System.out.println(Arrays.toString(secondArray));
		
		int[] thirdArray = getRandomArray(10);
		System.out.println(Arrays.toString(thirdArray));
		
		int[] fourthArray = Arrays.copyOf(thirdArray, 10);
		System.out.println(Arrays.toString(fourthArray));
		
		int[] smallArray = Arrays.copyOf(thirdArray, 5);
		System.out.println(Arrays.toString(smallArray));
		
		int[] largeArray = Arrays.copyOf(thirdArray, 15);
		System.out.println(Arrays.toString(largeArray));
		
		String[] str = {"Praba","karkuvel","Naruto","John cena","Obito"};
		if(Arrays.binarySearch(str, "Obito")>=0)
		{
			System.out.println("The Obito key is found");
		}
		
		int[] s = {1,2,3,4,5,6};
		int[] s1 = {1,2,3,4,5,6};
		if(Arrays.equals(s, s1))
		{
			System.out.println("Arrays are Equal");
		}
		else
		{
			System.out.println("Arrays are Not Equal");
		}
		
		

	}
	public static int[] getRandomArray(int len)
	{
		Random random  = new Random();
		int[] newArray = new int[len];
		for(int i =0; i<len; i++)
		{
			newArray[i] = random.nextInt(100);
		}
		return newArray;
		
	}

}
