package Arrays.utilArrays;

import java.util.Arrays;

public class ObjectDemo 
{

	public static void main(String[] args) 
	{
		int[] array = {20,20,32,22,1,49};
		Object obj = array;
		obj = "hello";
		if(obj instanceof String)
		{
			System.out.println("Yes it is True");
		}
		Object[] obj1 = new Object[3];
		obj1[0] = new StringBuilder("Hello");
		obj1[1] = array;
		obj1[2] = "Hello";
		for(Object obj11: obj1)
		{
			System.out.println(obj11);
		}
		
	}

}
