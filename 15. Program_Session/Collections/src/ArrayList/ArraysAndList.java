package ArrayList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArraysAndList 
{

	public static void main(String[] args) 
	{
//		var is same as List interface so we can't add the value 
		String[] str = new String[] {"First","Second","Third"};
//		var arrayList = Arrays.asList(str);
		List<String> arrayList = Arrays.asList(str);
		arrayList.set(0, "One");
//		arrayList.add("Praba");
		System.out.println("Arrays => " +Arrays.toString(str));
		System.out.println("List => " +arrayList);

	}

}
