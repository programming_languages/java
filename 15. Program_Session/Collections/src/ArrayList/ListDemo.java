package ArrayList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class ListDemo 
{

	public static void main(String[] args) 
	{
		String[] str = {"Praba","Naruto","Kakashi","Karkuvel"};
		List<String> list = List.of(str);
		System.out.println(list);
		System.out.println(list.getClass().getName());
//		list.add("Obito");  Adding Element in the list is not possible because is immutable
		
		ArrayList<String> nameList = new ArrayList<>(List.of(str));
		nameList.add("Obito");
		System.out.println(nameList);
		
		ArrayList<String> subNameList = new ArrayList<>(List.of("Minato","Pain","Itachi"));
		System.out.println(subNameList);
		
		nameList.addAll(subNameList);
		System.out.println(nameList);
		
		System.out.println("Third Name is "+nameList.get(2));
		
		if(nameList.contains("Naruto"))
		{
			System.out.println("The Name Naruto is found");
		}
		nameList.add("Naruto");
		System.out.println("The First Index = "+nameList.indexOf("Naruto"));
		System.out.println("The Last Index = "+nameList.lastIndexOf("Naruto"));
		
		System.out.println(nameList);
		nameList.remove(3);
		System.out.println(nameList);
		nameList.remove("Naruto");
		System.out.println(nameList);
		
		nameList.removeAll(List.of("Pain","Minato","Kakashi"));
		System.out.println(nameList);
		
		nameList.retainAll(List.of("Itachi","Obito"));
		System.out.println(nameList);
		
		nameList.clear();
		System.out.println(nameList);
		System.out.println("Is Empty = "+nameList.isEmpty());
		
		nameList.addAll(List.of("Praba","Karkuvel"));
		nameList.addAll(Arrays.asList("Naruto","Obito"));
		System.out.println(nameList);
		
		nameList.sort(Comparator.naturalOrder());
		System.out.println(nameList);
		
		nameList.sort(Comparator.reverseOrder());
		System.out.println(nameList);
		String[] stringArray = nameList.toArray(new String[nameList.size()]); 
		System.out.println(Arrays.toString(stringArray));
		
		
		
		
	}

}
