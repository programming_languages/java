package ArrayList;

import java.util.ArrayList;
import java.util.Arrays;

record GrocerryList(String name, String type,int count)
{
	public GrocerryList(String name)
	{
		this(name, "Dairy", 1);
	}
}


public class ArrayListDemo 
{

	public static void main(String[] args) 
	{
		GrocerryList[] obj = new  GrocerryList[3];
		obj[0] = new GrocerryList("Milk");
		obj[1] = new GrocerryList("ButterMilk");
		obj[2] = new GrocerryList("Orange","Produce",5);
		
		ArrayList <GrocerryList> grocerryItems = new ArrayList<>();
		grocerryItems.add(new GrocerryList("milk"));
		grocerryItems.add(new GrocerryList("ButterMilk"));
		grocerryItems.add(new GrocerryList("Panner"));
//		grocerryItems.add(0,new GrocerryList("curd"));// add overload Method with index
		grocerryItems.set(0,new GrocerryList("curd"));
		grocerryItems.remove(1);
		System.out.println(grocerryItems);
		
		
	}

}
