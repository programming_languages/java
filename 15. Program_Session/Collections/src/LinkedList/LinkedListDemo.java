package LinkedList;

import java.util.LinkedList;
import java.util.ListIterator;

public class LinkedListDemo 
{
	//LinkedList Implements the DoubllyLinked List and it implements all unimplemented List Interface Methods
	//And it also implements the queue/Dequeue and Stack Methods.

	public static void main(String[] args) 
	{
//		LinkedList<String> placetoVist = new LinkedList<>();
		var placeToVist = new LinkedList<String>();
		placeToVist.add("Chennai");
		placeToVist.add(0,"Thirunelvelli");
		System.out.println(placeToVist);
		addMoreMethods(placeToVist);
//		removeMoreMethods(placeToVist);
//		getElement(placeToVist);
//		printIterator(placeToVist);
//		printIterator1(placeToVist);
//		printIterator2(placeToVist);
		testIterator(placeToVist);

	}
	private static void addMoreMethods(LinkedList<String> list)
	{
		list.addFirst("Kovilpatti");
		list.addLast("Madurai");
		System.out.println(list);
		
		//QueueMethod 
		list.offer("Vellore");
		System.out.println(list);
		list.offerFirst("Vadapallani");
		list.offerLast("Navallur");
		System.out.println(list);
		
		//StackMethod
		list.push("VadaChennai");
		System.out.println(list);
	}
	private static  void removeMoreMethods(LinkedList<String> list)
	{
		System.out.println(list);
		list.remove(0);
		list.remove("Vadapallani");
		System.out.println(list);
		
		String s1 = list.removeFirst();
		String s2 = list.removeLast();
		System.out.println(s1+" was Removed");
		System.out.println(s2+" was Removed");
		
		System.out.println(list);
		
		//QueueMethod
		String p1 = list.poll();
		System.out.println(p1+" was Removed");
		
		System.out.println(list);
		String p2 = list.pollFirst();
		String p3 = list.pollLast();
		System.out.println(p2+" was Removed");
		System.out.println(p3+" was Removed");
		
		System.out.println(list);
		
		list.push("Delhi");
		list.push("Banglore");
		list.push("gujarat");
		System.out.println(list);
		
		//Stack Method
		String p4 = list.pop();
		System.out.println(p4+" was Removed");
		
		System.out.println(p4);
	}
	private static void getElement(LinkedList<String> list)
	{
		System.out.println(list);
		
		System.out.println("The Position at 1 is " + list.get(0));
		System.out.println("The Position at first is " + list.getFirst());
		System.out.println("The Position at Lats is "+list.getLast());
		
		//Queue Method 
		System.out.println("The Postition of Element() " + list.element());
		
		//Stack Method
		System.out.println("The Position of Peek()" + list.peek());
		System.out.println("The Position of First is " +list.peekFirst());
		System.out.println("The Position of Last is "+list.peekLast());
	}
	private static void printIterator(LinkedList<String> list)
	{
		System.out.println("The Trip is Start From "+list.getFirst());
		for(int i =1;i<list.size();i++)
		{
			System.out.println("===>  From " + list.get(i-1) + " to "+list.get(i));
		}
		System.out.println("The Trip is End From "+list.getLast() );
	}
	private static void printIterator1(LinkedList<String> list)
	{
		System.out.println("The Trip is Start From "+list.getFirst());
		String previousTown = list.getFirst();
		for(String town : list)
		{
			System.out.println("===>  From " + previousTown + " to "+ town);
			previousTown = town;
		}
		System.out.println("The Trip is End From "+list.getLast() );
	}
	private static void printIterator2(LinkedList<String> list)
	{
		System.out.println("The Trip is Start From "+list.getFirst());
		String previousTown = list.getFirst();
		ListIterator<String> iterator = list.listIterator(1);
		while(iterator.hasNext())
		{
			var town = iterator.next();
			System.out.println("===>  From " + previousTown + " to "+ town);
			previousTown = town;
		}
		System.out.println("The Trip is End From "+list.getLast() );
	}
	private static void testIterator(LinkedList<String> list)
	{
		var iterator = list.listIterator();
		while(iterator.hasNext())
		{
			if(iterator.next().equals("Chennai"))
			{
				iterator.add("praba");
			}
		}
		while(iterator.hasPrevious())
		{
			System.out.println(iterator.previous());
		}
		System.out.println(list);
		
		var iterator2 = list.listIterator(3);
        System.out.println(iterator2.previous());
	}

}
