package Challenges;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class GrocerryApplication 
{
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) 
	{
		boolean isAction = true;
		ArrayList<String> grocerryItems = new ArrayList<>();
		while(isAction)
		{
			System.out.println("0 - to shutdown");
			System.out.println("1 - to add item(s) to list (Comma delimited list)");
			System.out.println("2 - to remove item(s) to list (Comma delimited list)");
			System.out.println("Enter a number for which action you want to do ");
			switch(Integer.parseInt(sc.nextLine()))
			{
				case 0:
					isAction = false;
					break;
				case 1:
					addItems(grocerryItems);
					break;
				case 2:
					removeItems(grocerryItems);
					break;
					
			}
			Comparator.naturalOrder();
			System.out.println(grocerryItems);
			
		}
	}
	private static void addItems(ArrayList<String> grocerryItems)
	{
		System.out.println("Add item(s) to list (Comma delimited list)");
		String[] items = sc.nextLine().split(",");
		for(String trimed : items)
		{
			if(grocerryItems.indexOf(trimed.trim()) < 0)
			{
				grocerryItems.add(trimed);
			}
		}
		
		
	}
	private static void removeItems(ArrayList<String> grocerryItems)
	{
		System.out.println("Remove item(s) to list (Comma delimited list)");
		String[] removeItems = sc.nextLine().split(",");
		for(String trimed: removeItems)
		{
			if(grocerryItems.indexOf(trimed.trim())>0)
			{
				grocerryItems.remove(trimed);
			}
		}
	}

}
