package Challenges;

import java.util.Arrays;
import java.util.Scanner;

public class FindMinValue 
{

	public static void main(String[] args) 
	{
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number and seperated with comma:");
		String input = sc.next();
		String[] str  = input.split(",");
		int[] value  = new int[str.length];
		for(int i = 0;i<str.length;i++)
		{
			value[i] = Integer.parseInt(str[i].trim());
		}
		findMin(value);
	}
	private static void findMin(int... arr)
	{		
		int min = arr[0];
		for(int i = 1;i<arr.length;i++)
		{
			if(arr[i] < min)
			{
				min = arr[i];
			}
		}
		System.out.println("The Minimum Value is " + min);
	}

}
