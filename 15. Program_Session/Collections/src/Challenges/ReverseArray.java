package Challenges;

import java.util.Arrays;
import java.util.Random;

public class ReverseArray 
{

	public static void main(String[] args) 
	{
		int[] arr = getRandomArray(10);
		Arrays.sort(arr);
		System.out.println(Arrays.toString(arr));
		int[] resultArray = new int[arr.length];
		int no = 0;
		for(int i = arr.length-1;i>=0;i--)
		{
			resultArray[no] = arr[i];
			no++;
		}
		System.out.println(Arrays.toString(resultArray));

	}
	public static int[] getRandomArray(int len)
	{
		Random random = new Random();
		int[] newArray = new int[len];
		for(int i = 0;i<len;i++)
		{
			newArray[i] = random.nextInt(100);
		}
		return newArray;
	}

}
