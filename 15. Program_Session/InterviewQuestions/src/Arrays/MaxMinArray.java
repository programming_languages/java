package Arrays;

import java.util.Arrays;

public class MaxMinArray 
{

	public static void main(String[] args) 
	{
		MaxMinArray mma = new MaxMinArray();
		int[] arr = {2,3,4,5,1,6,78};
		System.out.println(mma.maximumArray(arr));
		System.out.println(mma.minimumArray(arr));
		System.out.println(mma.secondMaximum(arr));
		System.out.println(mma.secondMin(arr));

	}
	public int maximumArray(int[] arr)
	{
		int max = arr[0];
		for(int i = 1;i<arr.length;i++)
		{
			if(max < arr[i])
			{
				max = arr[i];
			}
		}
		return max;
	}
	public int minimumArray(int[] arr)
	{
		int min = arr[0];
		for(int i = 1;i<arr.length;i++)
		{
			if(min > arr[i])
			{
				min = arr[i];
			}
		}
		return min;
	}
	public int secondMaximum(int[] arr)
	{
		for(int i = 0;i<arr.length-1;i++)
		{
			for(int j= i+1;j<arr.length;j++)
			{
				int temp = 0;
				if(arr[i] > arr[j])
				{
					temp = arr[i];
					arr[i] =arr[j];
					arr[j] = temp;
				}
			}
		}
		int secondMax = arr[arr.length-2];
		return secondMax;
	}
	public int secondMin(int[] arr)
	{
		for(int i = 0;i<arr.length-1;i++)
		{
			for(int j= i+1;j<arr.length;j++)
			{
				int temp = 0;
				if(arr[i] > arr[j])
				{
					temp = arr[i];
					arr[i] =arr[j];
					arr[j] = temp;
				}
			}
		}
		int secondMinimum = arr[1];
		return secondMinimum;
	}

}
