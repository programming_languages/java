package Arrays;

import java.util.Arrays;

public class PrintingArray 
{

	public static void main(String[] args)
	{
		
		int[] array = {2,4,5,6,7,3,2,2};
		
		//Printing Array
		for(int i =0; i<array.length;i++)
		{
			System.out.print(array[i]+" ");
		}
		System.out.println();
		
		//Printing Array in Reverse Order
		for(int i = array.length-1;i>=0;i--)
		{
			System.out.print(array[i]+" ");
		}
		System.out.println();
		
		//Finding Index Of a Given Number
		int no = 7;
		for(int i =0;i<array.length;i++)
		{
			if(no == array[i])
			{
				System.out.println("The Index is "+i);
			}
		}
		

	}

}
