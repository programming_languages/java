package Arrays;

import java.util.Arrays;

public class MoveArray 
{

	public static void main(String[] args) 
	{
		MoveArray ma = new MoveArray();
		int[] arr = {1,2,3,4,5,6};
		System.out.println(Arrays.toString(arr));
		ma.moveLeft(arr);

	}
	public void moveRight(int[] arr)
	{
		int[] arr1 = new int[arr.length];
		int no =1;
		for(int i = 0;i<arr.length;i++)
		{
			if(i == arr.length-1)
			{
				arr1[0] = arr[i]; 
			}
			else
			{
				arr1[no] = arr[i];
				no++;
			}
		}
		System.out.println(Arrays.toString(arr1));
	}
	public void moveRight2(int[] arr)
	{
		int[] arr1 = new int[arr.length];
		int no =2;
		int temp = 0;
		for(int i = 0;i<arr.length;i++)
		{
			
			if(i == arr.length-2 || i == arr.length-1)
			{
				arr1[temp] = arr[i]; 
				temp++;
			}
			else
			{
				arr1[no] = arr[i];
				no++;
			}
		}
		System.out.println(Arrays.toString(arr1));
	}
	public void moveLeft(int[] arr)
	{
		int[] arr1 = new int[arr.length];
		int no = arr.length-2;
		for(int i =arr.length-1;i>=0;i--)
		{
			if(i == 0)
			{
				arr1[arr.length-1] = arr[i];
			}
			else
			{
				arr1[no] = arr[i];
				no--;
			}
		}
		System.out.println(Arrays.toString(arr1));
	}
	

}
