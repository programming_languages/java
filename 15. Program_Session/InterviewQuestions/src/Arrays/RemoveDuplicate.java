package Arrays;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class RemoveDuplicate 
{

	public static void main(String[] args) 
	{
		removeDuplicate();

	}
	public static boolean isPresent(ArrayList<String> country, String countryName)
	{
		boolean isThere = false;
		for(int i = 0; i<country.size();i++)
		{
			if(country.get(i).equals(countryName))
			{
				isThere = true;
			}
		}
		return isThere;
	}
	public static void removeDuplicate()
	{
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please Enter the Size Of the List");
		int size = scanner.nextInt();
		System.out.println("Enter the Element in the List");
		String[] arr = new String[size];
		for(int i =0; i<arr.length; i++)
		{
			arr[i] = scanner.next();
		}
		ArrayList<String> country = new ArrayList<>();
		for(int i = 0;i<arr.length;i++)
		{
			boolean isThere  = isPresent(country,arr[i]);
			if(isThere == false)
			{
 				country.add(arr[i]);
			}
		}
		HashMap<String, Integer> countryList = new HashMap<>();
		for(int i = 0; i<country.size();i++)
		{
			String countryName = country.get(i);
			int count = countCountry(countryName,arr);
			countryList.put(countryName, count);
			
		}
		System.out.println(countryList);
		
	}
	private static int countCountry(String countryName, String[] arr) 
	{
		int count  = 0;
		for(int i = 0; i<arr.length;i++)
		{
			if(arr[i].equals(countryName))
			{
				count++;
			}
		}
		return count;
	}

}
