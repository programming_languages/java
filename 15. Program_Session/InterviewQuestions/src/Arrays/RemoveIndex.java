package Arrays;
import java.util.Arrays;
public class RemoveIndex 
{

	public static void main(String[] args) 
	{
		RemoveIndex ri = new RemoveIndex();
		int[] arr = {1,2,3,4,6,5,7,89,90};
		int index = 4;
		ri.removeIndex(arr, index);

	}
	public void removeIndex(int[] arr,int index)
	{
		int[] arr1 = new int[arr.length-1];
		int no =0;
		for(int i =0;i<arr.length;i++)
		{
			if(i==index)
			{
				continue;
			}
			else
			{
				arr1[no] = arr[i];
				no++;
			}
		}
		System.out.println(Arrays.toString(arr1));
	}

}
