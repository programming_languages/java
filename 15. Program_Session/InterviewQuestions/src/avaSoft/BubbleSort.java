package avaSoft;

import java.util.Arrays;

public class BubbleSort 
{

	public static void main(String[] args) 
	{
		int[] arr = {4,5,6,3,2,1};
		arr = bubbleSort(arr);
		System.out.println(Arrays.toString(arr));

	}
	public static int[] bubbleSort(int[] arr)
	{
		for(int i =0; i<arr.length;i++)
		{
			int small = i;
			for(int j =i+1;j<arr.length;j++)
			{
				if(arr[i] > arr[j])
				{
					small = j;
					
				}
				int temp = arr[small];
				arr[small] = arr[i];
				arr[i] = temp;
			}
		}
		return arr;
	}
}
