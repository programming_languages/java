package loop_Statement.while_Loop;

//output
//5 5 5 5 5
//4 4 4 4 4 
//3 3 3 3 3 
//2 2 2 2 2
//1 1 1 1 1

public class Java_Pattern_2 
{

	public static void main(String[] args) 
	{
		int row = 5;
		while (row >= 1)
		{
		int col = 1;
		while(col <= 5)
		{
			System.out.print(row + " ");
			col = col + 1;
		}
		System.out.println();
		row = row - 1;
		}
		
		
		

	}

}
