package loop_Statement.while_Loop;

//output
//1 1 1 1 1 
//1 1 1 1 1 
//1 1 1 1 1 
//1 1 1 1 1 
//1 1 1 1 1 

public class Nested_While_Loop 
{

	public static void main(String[] args) 
	{
		int row = 1;
		
		while(row <= 5)
		{
			int col = 1;
			
			while (col <= 5)
			{
				System.out.print(1 +" ");
				col = col+1;
			}
			row = row + 1;
			System.out.println();
		}

	}

}
