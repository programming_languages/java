package loop_Statement.while_Loop.Task;

//output
// A B C D E 
//A B C D E 
//A B C D E 
//A B C D E 

public class Task_6 
{

	public static void main(String[] args) 
	{
		int row = 1;
		while(row <= 4)
		{
			int col = 65;
			while(col <= 69)
			{
				System.out.print( (char)col  + " ");
				col = col + 1;
			}
			row = row + 1;
			System.out.println();
		}

	}

}
