package loop_Statement.while_Loop.Task;

//output
//0 1 2 3 4

public class Task_4 
{

	public static void main(String[] args) 
	{
		int count = 0;
				
		while(count < 5)
		{
			System.out.print(count+ " ");
			count = count+1;
		}

	}

}
