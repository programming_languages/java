package loop_Statement.while_Loop.Task;

public class NumberSeries1 
{
	// 31, 62, 65, 260, 265, 1590
	public static void main(String[] args) 
	{
		int firstNumber = 31;
		System.out.print(firstNumber+" ");
		int no =1;
		int temp = 1;
		
		while(no <= 3)
		{
			firstNumber = firstNumber * temp + firstNumber;
			System.out.print(firstNumber+" ");
			temp += 2;
			firstNumber = firstNumber + temp;
			System.out.print(firstNumber+" ");
			no++;
		}
	}

}
