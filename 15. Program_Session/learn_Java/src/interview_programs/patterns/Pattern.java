package interview_programs.patterns;

public class Pattern 
{

	public static void main(String[] args) 
	{
		Pattern p = new Pattern();
		p.squareHallow(5);

	}
	public void squareHallow(int no)
	{
		for(int row = 0; row < no;row++)
		{
			for(int col = 0; col < no;col++)
			{
				if(row == 0 || col == 0 || row == no-1 || col == no-1)
				{
					System.out.print("*");
				}
				else
				{
					System.out.print(" ");
				}
				
			}
			System.out.println();
		}
	}

}
