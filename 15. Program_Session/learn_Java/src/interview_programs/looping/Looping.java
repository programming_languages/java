package interview_programs.looping;

public class Looping 
{

	public static void main(String[] args) 
	{
		Looping l = new Looping();
		l.simpleLoop1(5);

	}
	
	// 1 1 1 1 1
	public void simpleLoop(int no1)
	{
		for(int no = no1;no > 0;no--)
		{
			System.out.print("1 ");
		}
	}
	// 1 2 3 4 5
	public void simpleLoop1(int no1)
	{
		for(int no = no1;no > 0;no--)
		{
			System.out.print(no +" ");
		}
	}
	

}
