package programmingExercise.interviewQuestions;

public class ArrayProgram 
{

	public static void main(String[] args) 
	{
		ArrayProgram ap = new ArrayProgram();
		int[] array = {1,33,222,22,21};
		ap.maxMinArray(array);

	}
	public void  maxMinArray(int array[])
	{
		int max = 0;
		int min = array[1];
		
		for(int i = 0; i < array.length; i++)
		{
			if(array[i] > max)
			{
				max = array[i];
			}
		}
		
		for(int i = 0;i < array.length;i++)
		{
			if(array[i] < min)
			{
				min = array[i];
			}
		}
		
		System.out.println(max);
		System.out.println(min);
	}

}
