package programmingExercise.interviewQuestions;

public class StringPrograms 
{

		public static void main(String[] args) 
		{
			
			StringPrograms sp = new StringPrograms();
			
//			sp.palindromeString("Tat");
//			sp.checkVowels("Praba");
			sp.stringRemoveSpace("Karkuvel Prabakaran");
		}
		
		public StringBuffer reverseStringBuffer(String name)
		{
			char [] temp = name.toCharArray();
			
			StringBuffer reverse = new StringBuffer();
			for(int i = temp.length-1; i >= 0;i--)
			{
				 reverse.append(temp[i]);
			}
			System.out.println(reverse);
			
			return reverse;
		}
		public String reverseString(String name)
		{
			char [] temp = name.toCharArray();
			
			String reverse ="";
			
			for(int i = temp.length-1;i>=0;i--)
			{
				reverse = reverse + temp[i];
			}
			return reverse;
		}
		public void palindromeString(String name)
		{
			String temp = this.reverseString(name);
			
			if(name.equalsIgnoreCase(temp))
			{
				System.out.println("Is Palindrome");
			}
			else
			{
				System.out.println("Is Not Palindrome");
			}
			
		}
		public void checkVowels(String name)
		{
			boolean isVowels = false;

			for(int i = 0; i <= name.length()-1; i++)
			{
				if( name.charAt(i) == 'a' || name.charAt(i) == 'e' || name.charAt(i) == 'i' || name.charAt(i) == 'o' || name.charAt(i) == 'u')
				{
					System.out.println("Vowels Are Present");
					isVowels = true;
					break;
				}
				
			}
			if(isVowels == false)
			{
				System.out.println("Vowels Are Not Present");
			}
			
		}
		public void stringRemoveSpace(String name)
		{
			char[] temp = name.toCharArray();
			String rename = "";
			for(int i = 0; i <= temp.length-1; i++)
			{
				if(!Character.isWhitespace(temp[i]))
				{
					rename = rename + temp[i];
				}
			}
			System.out.println(rename);
		}

}
