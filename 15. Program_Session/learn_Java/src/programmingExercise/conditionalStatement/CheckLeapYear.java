package programmingExercise.conditionalStatement;

import java.util.Scanner;

public class CheckLeapYear 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		
		CheckLeapYear cly = new CheckLeapYear();
		
		System.out.print("Enter The Year: ");
		
		int no = sc.nextInt();
		
		cly.checkLeapYearOrNot(no);

	}
	public void checkLeapYearOrNot(int no)
	{
		if(no%400==0)
		{
			System.out.println("Leap Year");
		}
		else if(no%4==0 || no%400 == 0)
		{
			System.out.println("Leap Year");
		}
		else if(no%4==0)
		{
			System.out.println("Leap Year");
		}
		else
		{
			System.out.println("Not a Leap Year");
		}
	}

}
