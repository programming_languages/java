package learnJDBC;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCDemo 
{

	public static void main(String[] args) throws SQLException 
	{
		JDBCDemo jd  = new JDBCDemo();
//		jd.readData();
//		jd.insertData();
//		jd.updateData();
		jd.deleteData();

	}
	
	public void insertData() throws SQLException
	{
		String url = "jdbc:postgresql://localhost:5432/learnjdbc";
		String userName = "postgres";
		String password = "5321";
		String query = "insert into employee values (?,?,?)";
		
		Connection con = DriverManager.getConnection(url, userName, password);
		PreparedStatement ps = con.prepareStatement(query);
		ps.setInt(1, 2);
		ps.setString(2, "manoj");
		ps.setInt(3,45000);
		int rows = ps.executeUpdate();
		
		System.out.println("The No Of Rows Affected"+ rows);
		
		con.close();
		
		
	}
	public void readData() throws SQLException
	{
		String url = "jdbc:postgresql://localhost:5432/learnjdbc";
		String userName = "postgres";
		String password = "5321";
		String query = "select * from employee;";
	
		
		Connection con = DriverManager.getConnection(url, userName, password);
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
		while(rs.next())
		{
			System.out.println("The ID is "+rs.getInt(1));
			System.out.println("The Name is "+rs.getString(2));
			System.out.println("The Salary is "+rs.getInt(3));
		}
		con.close();
		
	}
	
	public void updateData() throws SQLException
	{
		String url = "jdbc:postgresql://localhost:5432/learnjdbc";
		String userName = "postgres";
		String password = "5321";
		String query = "update employee set salary = ? where id = ?";
	
		
		Connection con = DriverManager.getConnection(url, userName, password);
		PreparedStatement ps = con.prepareStatement(query);
		ps.setInt(1, 150000);
		ps.setInt(2,1);
		int rows = ps.executeUpdate();
		
		System.out.println("The No Of Rows Affected "+ rows);
		
		con.close();
	}
	
	public void deleteData() throws SQLException
	{
		String url = "jdbc:postgresql://localhost:5432/learnjdbc";
		String userName = "postgres";
		String password = "5321";
		String query = "delete from employee where id = ?";
	
		
		Connection con = DriverManager.getConnection(url, userName, password);
		PreparedStatement ps = con.prepareStatement(query);
		ps.setInt(1, 2);
		int rows = ps.executeUpdate();
		
		System.out.println("The No Of Rows Affected "+ rows);
		
		con.close();
	}

}
