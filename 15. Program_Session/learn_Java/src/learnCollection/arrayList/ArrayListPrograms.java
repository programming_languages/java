package learnCollection.arrayList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class ArrayListPrograms 
{

	public static void main(String[] args) 
	{
		ArrayListPrograms alp = new ArrayListPrograms();
		alp.colors();

	}
	public <T> void colors()
	{
		
		Scanner sc =  new Scanner(System.in);
		
		System.out.println("Enter 5 color names: ");
		
		ArrayList<Object> colors = new ArrayList<Object>();
		
		//Add Elements in ArrayList
		colors.add(sc.nextLine());
		colors.add(sc.nextLine());
		colors.add(sc.nextLine());
		colors.add(sc.nextLine());
		colors.add(sc.nextLine());
		
		System.out.println("The Colors are "+colors);
		
//		for (Object object : colors) {
//			System.out.println(object);
//		}
		
		//Add At Specific Position
//		colors.add(0,"Black");
//		System.out.println("The Colors are "+colors);
		
		//iterate the ArrayList TO Print the Values
//		for(int i = 0; i<colors.size();i++)
//		{
//			System.out.println("The Colors are "+colors.get(i));
//		}
		
		//Retrive the Element At specific Postion
//		System.out.println("Retrive Element "+colors.get(0));
		
		//Update the element in ArrayList
//		System.out.println("Update 3rd Position "+colors.set(2, "Yellow") +" to Yellow");
//		System.out.println(colors);
		
//		//Remove Element At Specific Position
//		System.out.println("Remove The Third Position "+colors.remove(2));
//		System.out.println(colors);
		
//		Search Element In ArrayList or use inbuild funtion contains 
		
//		System.out.println("Enter The Element to Serach");
//		String name = sc.nextLine();
//		boolean isTrue = false;
//		
//		for(int i = 0;i<colors.size();i++)
//		{
//			if(colors.get(i).equals(name))
//			{
//				isTrue = true;
//				
//			}
//		}
//		if(isTrue = true)
//		{
//			System.out.println("True");
//		}
//		else
//		{
//			System.out.println("False");
//		}
		
		//Sort
//		Collections.sort(colors, null);
//		System.out.println(colors);
		
		//Copy One ArrayList to another
//		ArrayList<Object> colors2 = new ArrayList<Object>();
//		
//		Collections.copy(colors2, colors);
//		System.out.println(colors2);
		
		//Sublist the arrayList
//		System.out.println(colors.subList(0, 3));
		
		Collections.swap(colors, 0, 1);
		System.out.println(colors);
		
		
		
	}

}
