package learnCollection;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrayListDemo 
{

	public static void main(String[] args) 
	{
		ArrayList al = new ArrayList();
		
		//Adding Value 
		al.add("Praba");
		al.add("Praba");
		al.add(12345);
		al.add(234.33);
		al.add(true);
		
		System.out.println(al);
		
		System.out.println(al.contains("Praba"));
		System.out.println(al.get(0));
		System.out.println(al.isEmpty());
		System.out.println(al.remove(2));
		System.out.println(al);
		
		ArrayList al2 = new ArrayList();
		
		al2.addAll(al);
		al2.add(21);
		al2.add(23);
		System.out.println(al);
		System.out.println(al2);
		System.out.println(al2.containsAll(al));
		System.out.println("Retain all");
		al2.retainAll(al);
		System.out.println(al2);
		
		System.out.println(al2.equals(al));
		
		  System.out.println(al2.equals(al));
		  System.out.println(al2.indexOf("manoj"));
		  System.out.println(al2.lastIndexOf("manoj"));
		  al2.add("sarathkumar");
		  al2.add("vignesh");
		  al2.removeAll(al);
		  System.out.println(al2);
		  al2.addAll(al);
		  System.out.println("al "+al);
		  System.out.println("al2 "+al2);
		  System.out.println(al2.size());
		  al2.add(1, "prabakaran");
		  System.out.println(al2);
		  al2.set(0, "abc");
		  System.out.println(al2);
		  Object[] ar = al2.toArray();
		  System.out.println("ObjectArray");
		  System.out.println(Arrays.toString(ar));
//		  for (Object ob : ar) {
//		   System.out.println(ob);
//		  }
		  
		  
		

	}

}
