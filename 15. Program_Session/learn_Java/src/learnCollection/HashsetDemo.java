package learnCollection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class HashsetDemo 
{

	public static void main(String[] args) 
	{
		ArrayList al = new ArrayList();
		
		al.add("Selve");
		al.add("Manoj");
		al.add("Monish");
		al.add("Vignesh");
		al.add("Vignesh");
//		al.add(100);
		
		System.out.println(al);
		
//		HashSet hs = new HashSet(al);
		
		HashSet hs = new HashSet();
		
		hs.addAll(al);
		
		System.out.println(hs);
		
		LinkedHashSet lhs = new LinkedHashSet();
		
		lhs.addAll(al);
		
		System.out.println(lhs);
		
		System.out.println(lhs);
		
		TreeSet ts = new TreeSet();
		
		ts.addAll(al);
		
		System.out.println(ts);
		
		
	}

}
