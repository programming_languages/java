package learnCollection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class MapDemo 
{

	public static void main(String[] args) 
	{
		
		HashMap hm = new HashMap();
		
		hm.put("idly",20);
		hm.put("Poori", 30);
		hm.put("Curd", 30);
		hm.put("Vadai", 30);
		hm.put("Dosai", 50);
		hm.put("Dosai", 50); // Map Not allow duplicates so it update the value.
		
		System.out.println(hm);
		System.out.println(hm.get("Idly"));
		System.out.println(hm.containsKey("dosai")); //Return Boolean Value
		System.out.println(hm.containsValue(50)); //Return Boolean Value
		System.out.println(hm.remove("poori")); //Returns Key Value
		System.out.println(hm);
		System.out.println(hm.replace("Dosai", 20));
		System.out.println(hm);
		System.out.println(hm.entrySet());
		System.out.println(hm.keySet());
		System.out.println(hm.values());
		
		
//		Set s = hm.entrySet();
//		
//		Iterator i = s.iterator();
//		
//		while(i.hasNext())
//		{
//			System.out.println(i.next());
//		}
		
		Set s = hm.entrySet();
		
		Iterator i = s.iterator();
		
		while(i.hasNext())
		{
			Map.Entry me = (Map.Entry)i.next();
			System.out.println(me.getKey()+ "Cost" + me.getValue() );
			
			if(me.getClass().equals("curd"))
			{
				me.setValue(40);
				me.setValue((int)me.getValue()+5);
				
			}
			
		}
		System.out.println(hm);
		
	}

}
