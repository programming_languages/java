package contest;

import java.util.Arrays;
import java.util.Scanner;

public class Solution 
{

    public static void main(String[] args) 
    {
        Scanner sc = new Scanner(System.in);
        int size = sc.nextInt();
        int[] no = new int[size];
        for(int i = 0;i<size;i++)
        {
            no[i] = sc.nextInt();
        }
        int target = sc.nextInt();
        Solution s = new Solution();
        System.out.println(Arrays.toString(s.twoSum(no, target)));
        
        
    }
    public int[] twoSum(int[] no, int target)
    {
        int arr[] = new int[2];
        
        for(int i =0; i<no.length; i++)
        {
            for(int j = i+1; j<no.length; j++)
            {
               

                	if(no[i]+no[j] == target){
                        arr[0] = i;
                        arr[1] = j;
                        return arr;
                    }
                	
                }
            
        }
       
        return new int[]{};
    }
}
