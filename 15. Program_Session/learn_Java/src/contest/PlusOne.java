package contest;

import java.util.Arrays;
import java.util.Scanner;

public class PlusOne {

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
        int size = sc.nextInt();
        int[] no = new int[size];
        for(int i = 0;i<size;i++)
        {
            no[i] = sc.nextInt();
        }
        PlusOne p = new PlusOne();  
        System.out.println(Arrays.toString(p.pulsOne(no)));

	}
	public int[] pulsOne(int[] no)
	{
		String str ="";
		for(int i = 0;i<no.length;i++)
		{
			str = str + no[i];
		}
		int no1 = Integer.parseInt(str);
		int sum = no1 + 1;
		String str2 = String.valueOf(sum);
		char[] ch  = str2.toCharArray();
		int[] ans  = new int[ch.length];
		for(int i = 0;i<ch.length;i++)
		{
			String st = String.valueOf(ch[i]);
			int n = Integer.parseInt(st);
			ans[i] = n;
		}
		return ans;
	}

}
