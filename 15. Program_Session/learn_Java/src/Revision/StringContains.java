package Revision;

public class StringContains 
{

	public static void main(String[] args) 
	{
		StringContains sc = new StringContains();
		String str = "123Java";
		String str1 = "a";
		System.out.println(sc.containString(str, str1));

	}
	public int containString(String str, String str1)
	{
		char[] ch = str.toCharArray();
		char[] ch1 = str1.toCharArray();
		int index =0;
		boolean isThere = false;
		for(int i =0;i<ch.length;i++)
		{
			if(ch[i] == ch1[0])
			{
				int temp = i;
				int count = 0;
				for(int j = 0;j<ch1.length;j++)
				{
					if(ch[temp] == ch1[j])
					{
						count++;
						temp++;
					}
				}
				if(count == ch1.length)
				{
					index = i;
					isThere = true;
					break;
				}
			}
		}

		if(!isThere)
		{
			return -1;
		}
		return index;
	}

}
