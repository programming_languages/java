package Revision;

public class Student 
{
	 Student(String name, int rollNo) {
		super();
		this.name = name;
		this.rollNo = rollNo;
	}
	private String name;
	private int rollNo;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getRollNo() {
		return rollNo;
	}
	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}
	public void displayBusInfo(){
		System.out.println("Student name:" + name + " Roll No:" + rollNo );
	}
}
