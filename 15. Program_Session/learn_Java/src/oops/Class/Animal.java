package oops.Class;

public class Animal 
{
	String name = "Peacoack";
	
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException 
	{
//		using new keyword
//		Animal object = new Animal();
//		System.out.println(object.name);
		
		Class cls = Class.forName("Animal");
		
		Animal obj = (Animal)cls.newInstance();
		
		System.out.println(obj.name);
		
	
	}

}
