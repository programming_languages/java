package sorting;

import java.util.Arrays;

public class SelectionSort 
{

	public static void main(String[] args) 
	{
		int[] arr = {8,7,6,3,2,1};
		for(int i = 0; i<arr.length-1;i++)
		{
			int small = i;
			for(int j =i+1; j<arr.length;j++)
			{
				if(arr[j]<arr[small])
					small = j; 
			}
			int temp = arr[small];
			arr[small] = arr[i];
			arr[i] = temp;
			
		}
		System.out.println(Arrays.toString(arr));

	}

}
