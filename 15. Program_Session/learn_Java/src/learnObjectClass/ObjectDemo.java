package learnObjectClass;

public class ObjectDemo 
{

	public static void main(String[] args) 
	{
		ObjectDemo od = new ObjectDemo();
		ObjectDemo od1 = new ObjectDemo();
		ObjectDemo od2 = od1;
		
		System.out.println(od.hashCode());
		System.out.println(od2.hashCode());
		
		//It will used equal the hashCode address
		System.out.println(od.equals(od2));
		System.out.println(od2.equals(od2));
		
	}
	
	//OverWrite the method from Object class
	public int hashCode()
	{
		return 10;
	}
}
