package learnMultiThreading;

public class ThreadDemo 
{
	public static void main(String[] args) 
	{
		//Normal Method Run and After Executing the for Loop
//		ThreadChild tc = new ThreadChild();
//		tc.run();
//		for(int i =1;i<=5;i++)
//		{
//			System.out.println("ThreadDemo" +i);
//		}
		
		
		ThreadChild tc = new ThreadChild();
		tc.start();
		
		ThreadChild tc2 = new ThreadChild();
		tc2.start();
		
		
		System.out.println(tc.getId());
		System.out.println(tc2.getId());
		
		System.out.println(tc.getName());
		System.out.println(tc2.getName());
		
		System.out.println(tc.getPriority());
		System.out.println(tc2.getPriority());
		
		System.out.println(tc.isAlive());
		System.out.println(tc2.isAlive());
		
		System.out.println(tc.isDaemon());
		System.out.println(tc2.isDaemon());
		
 		for(int i =1;i<=5;i++)
		{
			System.out.println("ThreadDemo" +i);
		}
		
	}
}
