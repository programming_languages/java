package leetCode;

import java.util.ArrayList;

public class GreatestCandies 
{

	public static void main(String[] args) 
	{
		GreatestCandies gc = new GreatestCandies();
		int[] candies = {2,3,5,1,3};
		int extraCandies = 3;
		gc.kidsWithCandies(candies, extraCandies);

	}
	public ArrayList<Boolean> kidsWithCandies(int[] candies, int extraCandies)
	{
		ArrayList<Boolean> ans = new ArrayList<Boolean>();
	    
		boolean isBig = false;
		for(int i = 0;i<candies.length;i++)
		{
			int big = candies[i]+extraCandies;
			for(int j = 0;j<candies.length;j++)
			{
				if(big > candies[j])
				{
					isBig = true;
				}
				else if(big == candies[j])
				{
					isBig = true;
				}
				else
				{
					isBig = false;
					break;
				}
			}
			if(isBig == true)
			{
				ans.add(true);
			}
			else
			{
				ans.add(false);
			}
		}
		System.out.println(ans);
		return ans;   
	}

}
