package leetCode;

public class ShuffleString 
{

	public static void main(String[] args) 
	{
		ShuffleString ss = new ShuffleString();
		String s ="codeleet";
		int[] indices = {4,5,6,7,0,2,1,3};
		ss.restoreString(s, indices);

	}
	public String restoreString(String s, int[] indices) 
	{
		char ch [] = new char[s.length()];
		for(int i = 0;i<s.length();i++)
		{
			ch[indices[i]] = s.charAt(i);
		}
		
		String ans = "";
		for(int i = 0;i<ch.length;i++)
		{
			ans = ans + ch[i];
		}
		System.out.println(ans);
		return ans;
    }
}
