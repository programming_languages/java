package leetCode;

public class ExactKElement 
{

	public static void main(String[] args) 
	{
		ExactKElement ee = new ExactKElement();
		int[] nums = {5,5,5};
		int key = 2;
		ee.maximizeSum(nums, key);

	}
	public int maximizeSum(int[] nums, int k) 
	{
		int sum = 0;
		int max =0;
		for(int i =0; i<nums.length;i++)
		{
			if(nums[i] > max)
			{
				max = nums[i];
			}
		}
		for(int i =0; i<k;i++)
		{
			sum = sum + max;
			max = max +1;
		}
		System.out.println(sum);
		return sum;
    }

}
