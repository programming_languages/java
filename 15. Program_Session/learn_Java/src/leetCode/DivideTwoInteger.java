package leetCode;

public class DivideTwoInteger 
{

	public static void main(String[] args) 
	{
		DivideTwoInteger dti = new DivideTwoInteger();
		int dividend = -2147483648, divisor = -1;
		System.out.println(dti.divide(dividend, divisor));
	}
	public int divide(int dividend, int divisor) 
	{
		if(dividend<0 && divisor<0)
		{
			return Math.abs(dividend) -1;
		}
        return dividend/divisor;
    }

}
