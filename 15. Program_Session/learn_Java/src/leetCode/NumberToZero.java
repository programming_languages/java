package leetCode;

public class NumberToZero 
{

	public static void main(String[] args) 
	{
		NumberToZero ntz = new NumberToZero();
		int num = 123;
		ntz.numberOfSteps(num);

	}
	public int numberOfSteps(int num) 
	{
		int count =0;
		while(true)
		{
			if(num == 0)
			{
				break;
			}
			if(num % 2 == 0)
			{
				num /= 2;
				count++;
			}
			else if(num % 2 != 0)
			{
				num = num -1;
				count++;
			}
		}
		System.out.println(count);
		return count;
	}

}
