package leetCode;

public class MaxNumberWords 
{

	public static void main(String[] args) 
	{
		MaxNumberWords mnw = new MaxNumberWords();
		String[] sentences = {"please wait", "continue to fight", "continue to win"};
		mnw.mostWordsFound(sentences);
	}
	
	public int mostWordsFound(String[] sentences) 
	{
		int max = 0;
		for(int i = 0;i<sentences.length;i++)
		{
			int no = space(sentences[i]);
			if(no > max)
			{
				max = no;
			}
		}
		System.out.println(max);
        return max;
    }
	
	public int space(String str)
	{
		int space = 0;
		char[] ch =  str.toCharArray();
		for(int i=0; i<ch.length;i++)
		{
			if(ch[i] == ' ')
			{
				space++;
			}
		}
		return space+1;
	}
}
