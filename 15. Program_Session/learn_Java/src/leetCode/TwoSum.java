package leetCode;

public class TwoSum 
{

	public static void main(String[] args) 
	{
		TwoSum ts = new TwoSum();
		int[] nums = {3,3};
		int target =6;
		ts.twoSum(nums, target);

	}
	public int[] twoSum(int[] nums, int target) 
	{
	   int res[] = new int[2];
	   for(int i = 0;i<nums.length;i++)
	   {
		   for(int j=0;j<nums.length;j++)
		   {
			 if(i != j)
			 {
				 if((nums[i] + nums[j]) == target)
				 {
					 res[0] = j;
					 res[1] = i;
				 }
			 }
		   }
	   }
	   System.out.println(res[0]);
	   System.out.println(res[1]);
	   return res;
	}

}
