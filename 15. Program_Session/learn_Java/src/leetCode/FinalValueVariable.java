package leetCode;

public class FinalValueVariable 
{

	public static void main(String[] args) 
	{
		FinalValueVariable fvv = new FinalValueVariable();
		String[] operations = {"X++","++X","--X","X--"};
		fvv.finalValueAfterOperations(operations);

	}
	public int finalValueAfterOperations(String[] operations) 
	{
		int count = 0;
		for(int i = 0;i<operations.length;i++)
		{
			if(operations[i].equals("X++"))
			{
				count = count + 1;
			}
			else if(operations[i].equals("++X"))
			{
				count = count + 1;
			}
			else if(operations[i].equals("X--")) 
			{
				count = count -1;
			}
			else if(operations[i].equals("--X")) 
			{
				count = count -1;
			}
		}
		System.out.println(count);
	    return count;    
	}
}
