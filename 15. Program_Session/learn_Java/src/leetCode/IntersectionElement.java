package leetCode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ListIterator;

public class IntersectionElement 
{

	public static void main(String[] args) 
	{
		int[] nums1 = {4,9,5}, nums2 = {9,4,9,8,4};
		intersection(nums1, nums2);
	}
	public static int[] intersection(int[] nums1, int[] nums2) 
	{
		HashSet<Integer> set = new HashSet<>();
		for(int nums : nums1)
		{
			set.add(nums);
		}
		ArrayList<Integer> ans = new ArrayList<>();
		for(int nums : set)
		{
			for(int i = 0;i<nums2.length;i++)
			{
				if(nums == nums2[i])
				{
					ans.add(nums);
					break;
				}
			}
		}
		int[] arr = new int[ans.size()];
		for(int i =0;i<ans.size();i++)
		{
			arr[i] = ans.get(i);
		}
        return arr;
    }
}
