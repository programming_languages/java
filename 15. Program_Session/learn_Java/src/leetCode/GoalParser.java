package leetCode;

public class GoalParser 
{

	public static void main(String[] args) 
	{
		GoalParser gp = new GoalParser();
		String command = "G()()()()(al)";
		gp.interpret(command);
		
	}
	public String interpret(String command) 
	{
		char[] ch = command.toCharArray();
		String ans = "";
		for(int i =0;i<ch.length;i++)
		{
			if(ch[i] =='(' && ch[i+1] == ')')
			{
				ans = ans + "o";
			}
			else if(ch[i] != '(' && ch[i] != ')')
			{
				ans = ans + ch[i];
			}
		}
		System.out.println(ans);
        return ans;
    }

}
