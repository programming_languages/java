package leetCode;

public class DiffBtwSumDigit 
{

	public static void main(String[] args) 
	{
		DiffBtwSumDigit dsd = new DiffBtwSumDigit();
		int[] nums = {1,2,3,4};
		System.out.println(dsd.differenceOfSum(nums));
		
	}
	public int differenceOfSum(int[] nums) 
	{
		int diff = sumNumber(nums) - sumDigits(nums);
        return Math.abs(diff);
    }
	public int sumNumber(int[] nums)
	{
		int ans = 0;
		for(int i =0; i<nums.length; i++)
		{
			ans = ans + nums[i];
		}
		return ans;
	}
	public int sumDigits(int[] nums)
	{
		int ans = 0;
		for(int i = 0;i<nums.length;i++)
		{
			ans = ans + sumNumbers(nums[i]);
		}
		return ans;
	}
	public int sumNumbers(int no)
	{
		int ans = 0;
		while(no>0)
		{
			int rem = no%10;
		    ans = ans + rem;
			no = no/10;
		}
		return ans;
		
	}
}
