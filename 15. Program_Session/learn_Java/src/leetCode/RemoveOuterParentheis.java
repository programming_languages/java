package leetCode;

public class RemoveOuterParentheis 
{

	public static void main(String[] args) 
	{
		RemoveOuterParentheis rop = new RemoveOuterParentheis();
		String s = "(()())(())(()(()))";
		rop.removeOuterParentheses(s);
		
	}
	public String removeOuterParentheses(String s) 
	{
		String str = "";
		char[] ch = s.toCharArray(); 
		for(int i = 1;i<ch.length-1;i++)
		{
			if(ch[i] == '(' && ch[i+1] == ')')
			{
				str = str + "()";
			}
		}
        return str;
    }

}
