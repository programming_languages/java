package leetCode;

import java.util.Arrays;

public class DuplicateNumber 
{

	public static void main(String[] args) 
	{
		DuplicateNumber dn = new DuplicateNumber();
		int[] nums = {3,1,3,4,2};
		System.out.println(dn.findDuplicate(nums));
		
	}
	public int findDuplicate(int[] nums) 
	{
		int duplicate = 0;
		Arrays.sort(nums);
		for(int i = 0;i<nums.length;i++)
		{
			for(int j = i+1;j<nums.length;j++)
			{
				if(nums[i] == nums[j])
				{
					duplicate = nums[i]; 
				}
			}
		}
		return duplicate;
	}

}
