package leetCode;

public class ShuffleArray 
{

	public static void main(String[] args) 
	{
		ShuffleArray sa = new ShuffleArray();
		int[] nums = {2,5,1,3,4,7};
		sa.shuffle(nums, 3); 
		

	}
	 public int[] shuffle(int[] nums, int n) 
	 {
		 int[] arr1 = new int[n];
		 int[] arr2 = new int[n];
		 for(int i = 0;i<n;i++)
		 {
			 arr1[i] = nums[i];
		 }
		 int count =0;
		 for(int i = n;i<nums.length;i++)
		 {
			 arr2[count] = nums[i];
			 count++;
		 }
		 int [] shuffle = new int[n*2];
		 int temp = 0;
		 for(int i = 0;i<n;i++)
		 {
			 shuffle[temp] = arr1[i];
			 temp++;
			 shuffle[temp] = arr2[i];
			 temp++;
		 }
		 
	     return shuffle;
	     
	 }
}
