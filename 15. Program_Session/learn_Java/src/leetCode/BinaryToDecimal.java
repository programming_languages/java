package leetCode;

public class BinaryToDecimal 
{

	public static void main(String[] args) 
	{
		BinaryToDecimal bd = new BinaryToDecimal();
		int n = 1111;
		System.out.println(bd.reverseBits(n));
	}
	public int reverseBits(int n) 
	{
		int decimal = 0;
		int no =0;
		while(true)
		{
			if(n == 0)
			{
				break;
			}
			else
			{
				int temp = n%10;
				decimal += temp*Math.pow(2, no);
				no++;
				n = n/10;
				
			}
			
		}
        return decimal;
    }

}
