package leetCode;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrangeNumbers 
{
	public static void main(String[] args) 
	{
		ArrangeNumbers an = new ArrangeNumbers();
		int[] nums = {3,1,-2,-5,2,-4};
		an.rearrangeArray(nums);
	}
	public int[] rearrangeArray(int[] nums) 
	{
		int[] result = new int[nums.length];
 		ArrayList<Integer> pos = new ArrayList<Integer>();
 		ArrayList<Integer> neg = new ArrayList<Integer>();
 		for(int i = 0;i<nums.length;i++)
 		{
 			if(nums[i]>=0)
 			{
 				pos.add(nums[i]);
 			}
 			else
 			{
 				neg.add(nums[i]);
 			}
 		}
 		int no = 0;
 		for(int i = 0;i<pos.size();i++)
 		{
 			result[no] = pos.get(i);
 			result[no+1] = neg.get(i);
 			no = no + 2;
 		}
 		System.out.println(Arrays.toString(result));
        return result;
    }
}
