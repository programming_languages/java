package leetCode;

import java.util.Arrays;

public class TownJudge 
{

	public static void main(String[] args) 
	{
		TownJudge tj = new TownJudge();
		int n = 3;
	}
	public int findJudge(int n, int[][] trust) 
	{
          int trusted[]=new int[n+1];
          int nottrusted[]=new int[n+1];
          for(int arr[]:trust)
          {
        	  System.out.println(arr[0]);
        	  trusted[arr[0]]++;
        	  System.out.println(arr[1]);
        	  nottrusted[arr[1]]++;
          }
          System.out.println(Arrays.toString(trusted));
          System.out.println(Arrays.toString(nottrusted));
          for(int i=1;i<=n;i++)
          {
        	  if(trusted[i]==0 && nottrusted[i]==n-1)
        	  {
        		  return i;
        	  }
          }
          return -1;
	}

}
