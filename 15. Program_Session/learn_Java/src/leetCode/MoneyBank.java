package leetCode;

public class MoneyBank
{

	public static void main(String[] args) 
	{
		MoneyBank mb = new MoneyBank();
		int n = 20;
		mb.totalMoney(n);
	}
	public int totalMoney(int n) 
	{
	     int count = 0;
	     int no = 1;
	     int loop = 1;
	     int money = 0;
	     while(count < n)
	     {
	    	 no = 1*loop;
	    	 int week =1;
	    	 while(week <= 7 && count < n)
	    	 {
		    	 money = money + no;
		    	 count++;
		    	 no++;
		    	 week++;
	    	 }
	    	 loop++;
	     }
	     return money;
	}

}
