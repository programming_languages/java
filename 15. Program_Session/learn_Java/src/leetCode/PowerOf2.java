package leetCode;

public class PowerOf2 
{

	public static void main(String[] args) 
	{
		PowerOf2 obj = new PowerOf2();
		
		boolean isCorrect = obj.isPowerOfTwo(5);
		
		System.out.println(isCorrect);
	}
	public boolean isPowerOfTwo(int n) 
	{
        int result = 1;
		boolean check =false;
		while(true)
		{
			
			if(result <= n )
			{
				if(result == n)
				{
					check =true;
					break;
				}
				
			}
			else if(result > n)
			{
				break;
			}
			result = result * 2;
			n = n/2;
		}
		return check;
    }
}
