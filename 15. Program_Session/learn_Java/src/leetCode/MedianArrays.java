package leetCode;

import java.util.Collection;

public class MedianArrays 
{

	public static void main(String[] args) 
	{
		MedianArrays ma = new MedianArrays();
		int [] nums1 = {1,2};
		int [] nums2 = {3};
		ma.findMedianSortedArrays(nums1, nums2);

	}
	public double findMedianSortedArrays(int[] nums1, int[] nums2) 
	{
		int[] median = new int[nums1.length+nums2.length];
		
		for(int i = 0;i<nums1.length;i++)
		{
			median[i] = nums1[i];
		}
		for(int i = 0;i<nums2.length;i++)
		{
			median[i+nums1.length] = nums2[i];
		}
		 
		for(int i=0;i<median.length;i++)
		{
			int temp = 0;
			for(int j=i+1;j<median.length;j++)
			{
				if(median[i]>median[j])
				{
					temp = median[i];
					median[i] = median[j];
					median[j] = temp;
					
				}
			}
		}
		
		int length = median.length/2;
		double ans = 0;
		if(median.length%2==0)
		{
			ans = median[length-1] + median[length];
			ans = ans/2;
		}
		else
		{
			ans = median[length];
		}
		return ans;
	}
}
