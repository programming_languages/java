package leetCode;

import java.util.Arrays;

public class ArrayPermutation 
{

	public static void main(String[] args) 
	{
		ArrayPermutation ap = new ArrayPermutation();
		int[] nums = {5,0,1,2,3,4};
//		ap.buildArray(nums);
		System.out.println(Arrays.toString(ap.buildArray(nums)));
 	}
	public int[] buildArray(int[] nums) 
	{
		int[] ans  = new int[nums.length];
		for(int i =0;i<nums.length;i++)
		{
			ans[i] = nums[nums[i]];
		}
		return ans;
	}

}
