package leetCode;

public class AddDigit 
{

	public static void main(String[] args) 
	{
		AddDigit ad = new AddDigit(); 
		int num = 448;
		ad.addDigits(num);
	}
	public int addDigits(int num) 
	{
		int no = num;
        while(no > 9)
        {
        	 no =sumNumbers(no);
        }
        System.out.println(no);
        return no;
    }
	public int sumNumbers(int no)
	{
		int ans = 0;
		while(no>0)
		{
			int rem = no%10;
		    ans = ans + rem;
			no = no/10;
		}
		return ans;
		
	}
}
