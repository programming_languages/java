package leetCode;

public class Power4 
{

	public static void main(String[] args) 
	{
		Power4 p = new Power4();
		int n = 16;
		p.isPowerOfFour(n);
		
	}
    public boolean isPowerOfFour(int n) 
    {
    	if (n == 4 || n == 1) {
            return true;
        }
        if (n < 4  || n % 4 != 0) {
            return false;
        }

        return isPowerOfFour(n / 4);  
	}

}
