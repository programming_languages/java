package leetCode;

import java.util.ArrayList;

public class CountCommonWords 
{

	public static void main(String[] args) 
	{
		CountCommonWords ccw = new CountCommonWords();
		String[] words1 = {"a","ab"}; 
		String[] words2 = {"a","a","a","ab"};
		ccw.countWords(words1, words2);

	}
	public int countWords(String[] words1, String[] words2) 
	{
		ArrayList al = new ArrayList();
		ArrayList al1 = new ArrayList();
		int count = 0;
		for(int i = 0;i<words1.length;i++)
		{
			boolean isTrue = false;
			for(int j = 0; j<words1.length;j++)
			{
				if(i != j)
				{
					if(!words1[i].equals(words1[j]))
					{
						isTrue = true;
					}
					else
					{
						isTrue = false;
						break;
					}
				}
			}
			if(isTrue)
			{
				al.add(words1[i]);
			}
		}
		for(int i = 0;i<words2.length;i++)
		{
			boolean isTrue = false;
			for(int j = 0; j<words2.length;j++)
			{
				if(i != j)
				{
					if(!words2[i].equals(words2[j]))
					{
						isTrue = true;
					}
					else
					{
						isTrue = false;
						break;
					}
				}
			}
			if(isTrue)
			{
				al1.add(words2[i]);
			}
		}
		for(int i =0; i<al.size();i++)
		{
			for(int j = 0; j<al1.size(); j++)
			{
				if(al.get(i).equals(al1.get(j)))
				{
					count = count + 1;
				}
			}
		}
		System.out.println(count);
		return count;
	}
}
