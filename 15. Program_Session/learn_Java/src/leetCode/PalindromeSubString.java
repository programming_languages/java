package leetCode;

public class PalindromeSubString 
{

	public static void main(String[] args) 
	{
		PalindromeSubString pss = new PalindromeSubString();
		String s = "abc";
		System.out.println(pss.countSubstrings(s));
		
	}
	public int countSubstrings(String s) 
	{
		int count = 0;
		for(int i = 0;i<s.length();i++)
		{
			for(int j =i;j<s.length();j++)
			{
				if(isPalindrome(s, i, j))
				{
					count++;
				}
			}
		}
        return count;
    }
	public boolean isPalindrome(String s,int i, int j)
	{
		while(i<j)
		{
			if(s.charAt(i) != s.charAt(j))
			{
				return false;
			}
			i++;
			j--;
		}
		return true;
	}

}
