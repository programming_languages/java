package leetCode;

public class LRSumDiffrence {

	public static void main(String[] args) 
	{
		LRSumDiffrence lrs = new LRSumDiffrence();
		
		int[] nums = {10,4,8,3};
		
		lrs.leftRightDifference(nums);

	}
	public int[] leftRightDifference(int[] nums) 
	{
		int[] ans =  new int[nums.length];
		int[] lsum = new int[nums.length];
		int[] rsum = new int[nums.length];
		
		lsum[0] = 0;
		for(int i = 1; i <nums.length;i++)
		{
			lsum[i] = lsum[i-1]+nums[i-1];
			
		}
		int[] re = new int[nums.length];
		int length =nums.length-1;
		for(int i = 0;i<nums.length;i++)
		{
			re[i] = nums[length];
			length--;
		}
		rsum[0]=0;
		for(int i = 1;i<nums.length;i++)
		{
			rsum[i] = rsum[i-1]+re[i-1]; 
			
			
		}
		int count =nums.length-1;
		for(int i = 0;i<nums.length;i++)
		{
			re[count] = rsum[i];
			count--;
		}
		for(int i=0;i<ans.length;i++)
		{
			ans[i]=lsum[i]-re[i];
			
		}
		int no =0;
		for(int i=0;i<ans.length;i++)
		{
			no = ans[i];
			no = Math.abs(no);
			ans[i]=no;
			System.out.println(ans[i]);
		}
		return ans;
    }

}
