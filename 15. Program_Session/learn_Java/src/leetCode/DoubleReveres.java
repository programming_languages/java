package leetCode;

public class DoubleReveres 
{

	public static void main(String[] args) 
	{
		DoubleReveres dr = new DoubleReveres();
		dr.isSameAfterReversals(0);

	}
	public boolean isSameAfterReversals(int num) 
	{
		boolean isEqual = false;
		int no = 0;
		int temp = num;
		for(int i =1; i<=2;i++)
		{
			no = reverse(num);
			System.out.println(no);
			num = no;
		}
		if(temp == no)
		{
			isEqual = true;
		}
		System.out.println(isEqual);
		return isEqual;
	   
	}
	public int reverse(int no)
	{
		int reverse = 0;
		while(no > 0)
		{
			int rem = no % 10;
			reverse =  reverse * 10 + rem;
			no /= 10;
		}
		return reverse;
	}

}
