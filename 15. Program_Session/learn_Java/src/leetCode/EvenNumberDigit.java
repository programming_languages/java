package leetCode;

public class EvenNumberDigit 
{

	public static void main(String[] args) 
	{
		EvenNumberDigit end = new EvenNumberDigit();
		int[] nums = {555,901,482,1771};
		System.out.println(end.findNumbers(nums));

	}
	public int findNumbers(int[] nums) 
	{
		 int count = 0;
		 for(int i = 0;i<nums.length;i++)
		 {
			 int no = this.countNumber(nums[i]);
			 if(no%2 == 0)
			 {
				 count++;
			 }
		 }
	     return count;   
	}
	public int countNumber(int no)
	{
		int count = 0;
		int temp = no;
		while(no > 0)
		{
			no = no/10;
			count++;
		}
		return count;
	}
	

}
