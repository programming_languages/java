package leetCode;

public class SumMultiples 
{

	public static void main(String[] args) 
	{
		SumMultiples sm = new SumMultiples();
		int n = 10;
		sm.sumOfMultiples(n);
	}
	public int sumOfMultiples(int n) 
	{
		int sum =0;
		for(int i =1;i<=n ;i++)
		{
			if(i%3 == 0)
			{
				sum = sum + i;
			}
			else if(i%5 == 0)
			{
				sum = sum + i;
			}
			else if(i%7 == 0)
			{
				sum = sum + i;
			}
		}
		System.out.println(sum);
		return sum;
	   
	}

}
