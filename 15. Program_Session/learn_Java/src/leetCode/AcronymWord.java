package leetCode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AcronymWord 
{

	public static void main(String[] args) 
	{
		AcronymWord aw = new AcronymWord();
		String[] ar = {"never","gonna","give","up","on","you"};
		List<String> words = Arrays.asList(ar);
		String s = "ngguoy";
		aw.isAcronym(words, s);
	}
	public boolean isAcronym(List<String> words, String s) 
	{
		int no = 0;
		int count = 0;
		boolean isAcronym = false;
		if(words.size() == s.length())
		{	
			for(int i =0; i<words.size();i++)
			{
				String ref = words.get(i);
				if(ref.charAt(0) == s.charAt(no))
				{
					count++;
				}
				no++;
			}
			if(count == s.length())
			{
				isAcronym = true;
			}
		}
		System.out.println(isAcronym);
		return isAcronym;     
	}

}
