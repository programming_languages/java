package leetCode;

import java.util.ArrayList;

public class DivSum {

	public static void main(String[] args) 
	{
		DivSum ds = new DivSum();
		int n = 5, m = 1;
		ds.differenceOfSums(n, m);
	}
	public int differenceOfSums(int n, int m) 
	{
		ArrayList divisible = new ArrayList();
		ArrayList nonDivisible = new ArrayList();
		int no1 =0, no2 = 0;
		for(int i =1; i<=n; i++)
		{
			if(i % m == 0)
			{
				divisible.add(i);
			}
			else if(i % m != 0)
			{
				nonDivisible.add(i);
			}
		}
		for(int i =0;i<divisible.size();i++)
		{
			no1 = no1 + (int)divisible.get(i);
		}
		for(int i =0;i<nonDivisible.size();i++)
		{
			no2 = no2 + (int)nonDivisible.get(i);
		}
		int difference = no2 - no1;
		System.out.println(difference);
        return difference;
    }

}
