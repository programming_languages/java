package leetCode;

public class RichCustomer 
{

	public static void main(String[] args) 
	{
		RichCustomer rc = new RichCustomer();
		int[][] accounts =  {{2,8,7},{7,1,3},{1,9,5}};
		rc.maximumWealth(accounts);

	}
	public int maximumWealth(int[][] accounts) 
	{
		int max =0;
		for(int i =0;i<accounts.length;i++)
		{
			int no = 0;
			for(int j =0 ;j<accounts[i].length;j++)
			{
				no = no + accounts[i][j];
			}
			if(no > max)
			{
				max = no;
			}
		}
		System.out.println(max);
		return max;
	        
	}
}
