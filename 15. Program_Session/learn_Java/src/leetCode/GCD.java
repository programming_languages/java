package leetCode;

public class GCD 
{

	public static void main(String[] args) 
	{
		GCD gcd = new GCD();
		int[] nums = {3,3};
		gcd.findGCD(nums);
		
	}
	public int findGCD(int[] nums)
	{
		int min = nums[0];
		int max = nums[0];
		int gcd = 0;
		for(int i =1 ; i<nums.length;i++)
		{
			if(nums[i]<min)
			{
				min = nums[i];
			}
			if(nums[i]>max)
			{
				max = nums[i];
			}
		}
		for(int i = 1;i<=min;i++)
		{
			if(min % i == 0 && max % i ==0)
			{
				gcd = i;
			}
		}
		System.out.println(gcd);
		return gcd;
	  
	}

}
