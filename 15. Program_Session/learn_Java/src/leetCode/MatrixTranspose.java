package leetCode;

import java.util.Arrays;

public class MatrixTranspose 
{

	public static void main(String[] args) 
	{
		int[][] matrix = {{1,2,3},{4,5,6},{7,8,9}};
		transpose(matrix);

	}
	public static int[][] transpose(int[][] matrix) 
	{
		int[][] ans =  new int[matrix.length][matrix[0].length];
		int no = 0;
		for(int i =0; i<matrix.length;i++)
		{
			for(int j = 0;j<matrix[0].length;j++)
			{
				ans[j][no] = matrix[i][j];
				
			}
			no++;
		}
		return ans;
	}

}
