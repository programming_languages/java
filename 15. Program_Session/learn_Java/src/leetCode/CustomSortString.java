package leetCode;

import java.util.Arrays;

public class CustomSortString 
{

	public static void main(String[] args) 
	{
//		String order = "kqep", s = "pekeq";
		String order = "cba", s = "abcdbb" ;
		customSortString(order, s);

	}
	public static String customSortString(String order, String s) 
	{
   		String output = "";
   		char[] arr = order.toCharArray();
   		char[] arr1 = s.toCharArray();
   		for(char ch : arr)
   		{
   			for(char ch1 : arr1)
   			{
   				if(ch == ch1)
   				{
   					output = output + ch;
   				}
   			}
   		}
   		char[] out = output.toCharArray();
   		for(char ch : arr1)
   		{
   			boolean isPresent = false;
   			for(char ch1 : out)
   			{
   				if(ch1 == ch)
   				{
   					isPresent = true;
   					break;
   				}
   			}
   			if(isPresent == false)
   			{
   				output = output + ch;
   			}
   		}
   		System.out.println(output);
   		return output;
    }

}
