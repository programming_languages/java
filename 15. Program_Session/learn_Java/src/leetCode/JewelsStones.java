package leetCode;

public class JewelsStones 
{

	public static void main(String[] args) 
	{
		JewelsStones js = new JewelsStones();
		String jewels = "zh", stones = "ZZzzHzzhidz";
		js.numJewelsInStones(jewels, stones);

	}
	public int numJewelsInStones(String jewels, String stones) 
	{
	    int count = 0;
	    char[] ch = jewels.toCharArray();
	    char[] ch1 = stones.toCharArray();
	    for(int i =0;i<ch.length;i++)
	    {
	    	for(int j =0;j<ch1.length;j++)
	    	{
	    		if(ch[i] == ch1[j])
	    		{
	    			count++;
	    		}
	    	}
	    }
	    System.out.println(count);
	    return count;
	}

}
