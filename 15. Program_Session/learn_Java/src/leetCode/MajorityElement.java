package leetCode;

public class MajorityElement 
{

	public static void main(String[] args) 
	{
		MajorityElement me = new MajorityElement();
		int[] nums = {2,2,1,1,1,2,2};
		System.out.println(me.majorityElement(nums));
		
	}
	public int majorityElement(int[] nums) 
	{
		int element = 0;
		int temp = 0;
		for(int i =0;i<nums.length;i++)
		{
			int count = 0;
			for(int j = i;j<nums.length;j++)
			{
				if(nums[i] == nums[j])
				{
					count++;
				}
			}
			if(count > temp)
			{
				temp = count;
				element = nums[i];
			}
		}
        return element;
    }
	

}
