package leetCode;

public class LengthLastWord 
{

	public static void main(String[] args) 
	{
		LengthLastWord llw = new LengthLastWord();
		String s ="a ";
		System.out.println(llw.lengthOfLastWord(s));
		
	}
	public int lengthOfLastWord(String s) 
	{
		
		int count = 0;
		boolean isSpace = false;
		char[] ch = s.toCharArray();
		for(int i =ch.length-1;i>=0;i--)
		{
			if(ch[i] != ' ')
			{
				count++;
				isSpace = true;
			}
			else if(isSpace == true)
			{
				break;
			}
			
		}
	    return count;
	}

}
