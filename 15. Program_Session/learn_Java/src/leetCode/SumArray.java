package leetCode;

public class SumArray {

	public static void main(String[] args) 
	{
		SumArray sa = new SumArray();
		int[] nums = {3,1,2,10};
		sa.runningSum(nums);	

	}
	public int[] runningSum(int[] nums) 
	{
        int [] ans = new int[nums.length];
        ans[0] = nums[0];
        
        for(int i = 1; i<nums.length;i++)
        {
        	ans[i] = ans[i-1]+nums[i];
        }
        for(int i = 0; i<ans.length;i++)
        {
        	System.out.println(ans[i]);
        }
        
        return ans;
    }
}
