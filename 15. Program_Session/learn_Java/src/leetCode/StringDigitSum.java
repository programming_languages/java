package leetCode;

import java.math.BigInteger;

public class StringDigitSum 
{

	public static void main(String[] args) 
	{
		StringDigitSum sds = new StringDigitSum();
		String s = "zbax";
		int k = 2;
		sds.getLucky(s,k);

	}
	public int getLucky(String s, int k) 
	{
		String str1 = "";
		int n = 1;
		BigInteger ans = new BigInteger("0");
		for(int i =0; i<s.length();i++)
		{
			String str = ""+s.charAt(i); 
			str1 = str1 + convertString(str); 
			
		}
		BigInteger no = new BigInteger(str1);
		
		while(n <= k)
		{
			ans = sumDigit(no);
			no = ans;
			n++;
		}
		int ans1 = ans.intValue();
		System.out.println(ans1);
		return ans1;
    }
	
	public int convertString(String s)
	{
		char[] ch = new char[26];
		int no = 65;
		int count = 0;
		String s1 = s.toUpperCase();
		for(int i = 0;i<ch.length;i++)
		{
			ch[i] = (char)no;
			no++;
		}
		for(int i = 0;i<ch.length;i++)
		{
			count++;
			if(s1.charAt(0) == ch[i])
			{
				break;
			}
		}
		return count;
	}
	public BigInteger sumDigit(BigInteger no )
	{
		BigInteger ans = new BigInteger("0");
		BigInteger n = new BigInteger("10");
		BigInteger div = new BigInteger("10");
		while(no.signum() == 1)
		{
			
			BigInteger rem = no.mod(n);
			ans = ans.add(rem);
			no  = no.divide(div);
		}
		return ans;
	}

}
