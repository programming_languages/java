package leetCode;

public class IPAddress 
{

	public static void main(String[] args) 
	{
		IPAddress ip = new IPAddress();
		String address = "1.1.1.1";
		ip.defangIPaddr(address);

	}
	public String defangIPaddr(String address) 
	{
		char[] ch = address.toCharArray();
		String ans = "";
		for(int i =0; i<ch.length; i++)
		{
			if(ch[i] == '.')
			{
				ans = ans +"[.]";
			}
			else
			{
				ans = ans + ch[i];
			}
		}
		return ans;
	}

}
