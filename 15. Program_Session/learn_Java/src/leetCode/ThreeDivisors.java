package leetCode;

public class ThreeDivisors 
{

	public static void main(String[] args) 
	{
		ThreeDivisors td = new ThreeDivisors();
		int n = 9;
		td.isThree(n);

	}
	public boolean isThree(int n) 
	{
		int count = 0;
		boolean isTrue = false;
		for(int i = 1;i<=n;i++)
		{
			if(n%i == 0)
			{
				count++;
			}
		}
		if(count == 3)
		{
			isTrue = true;
		}
		System.out.println(isTrue);
        return isTrue;
    }
}
