package leetCode;

public class Search2DArray
{

	public static void main(String[] args) 
	{
		int[][] matrix = {{1,4,7,11,15},{2,5,8,12,19},{3,6,9,16,22},{10,13,14,17,24},{18,21,23,26,30}};
		int target = 5;
		searchMatrix(matrix, target);

	}
	public static boolean searchMatrix(int[][] matrix, int target) 
	{
		boolean isPresent = false;
		for(int i = 0;i<matrix.length;i++)
		{
			for(int j =0;j<matrix[0].length;j++)
			{
				if(matrix[i][j] == target)
				{
					isPresent = true;
					break;
				}
			}
		}
		System.out.println(isPresent);
        return isPresent;
    }

}
