package leetCode;

public class OptainZero 
{

	public static void main(String[] args) 
	{
		OptainZero oz = new OptainZero();
		int num1 = 10 , num2 = 10;
		oz.countOperations(num1, num2);
		
	}
	public int countOperations(int num1, int num2) 
	{
		int count = 0;
		while(true)
		{
			if(num1 == 0 || num2 ==0)
			{
				break;
			}
			else if (num1 >= num2)
			{
				num1 = num1 - num2;
				count++;
			}
			else if(num2 >= num1)
			{
				num2 = num2 - num1;
				count++;
			}
		}
		System.out.println(count);
		return count;
	}
}
