package leetCode;

import java.util.Arrays;

public class RemoveElementTwoArray 
{

	public static void main(String[] args) 
	{
		RemoveElementTwoArray rea = new RemoveElementTwoArray();
		int[] nums1 = {4,3,2,3,1};
		int[] nums2 = {2,2,5,2,3,6};
		rea.findIntersectionValues(nums1, nums2);
	}
	 public int[] findIntersectionValues(int[] nums1, int[] nums2) 
	 {
		 int[] ans = new int[2];
		 int no = 0;
		 for(int i = 0;i<nums1.length;i++)
		 {
			 for(int j = 0;j<nums1.length;j++)
			 {
				 if(nums1[i] == nums2[j])
				 {
					 if(ans[0] != nums1[i] && ans[1] != nums1[i])
					 {
						 ans[no] = nums1[i];
						 no++;
					 }
				 }
			 }
		 }
		 System.out.println(Arrays.toString(ans));
		 return null;
	 }
}
