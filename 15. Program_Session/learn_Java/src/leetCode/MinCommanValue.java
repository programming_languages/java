package leetCode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
public class MinCommanValue 
{

	public static void main(String[] args) 
	{
		MinCommanValue mcv = new MinCommanValue();
		int[] nums1 = {1,2,3,6}, nums2 = {2,3,4,5};
		System.out.println(mcv.getCommon(nums1, nums2));

	}
	public int getCommon(int[] nums1, int[] nums2) 
	{
		  HashSet<Integer> set = new HashSet<>();
	        for (int num : nums1) {
	            set.add(num);
	        }
	        for (int num : nums2) {
	            if (set.contains(num)) {
	                return num;
	            }
	        }
	        return -1;
    }

}
