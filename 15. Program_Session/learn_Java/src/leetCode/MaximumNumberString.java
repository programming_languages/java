package leetCode;

public class MaximumNumberString 
{

	public static void main(String[] args) 
	{
		MaximumNumberString mns = new MaximumNumberString();
		String[]  words = {"cd","ac","dc","ca","zz"};
		System.out.println(mns.maximumNumberOfStringPairs(words));
	}
	public int maximumNumberOfStringPairs(String[] words) 
	{
		int count = 0;
		for(String str : words)
		{
			int no =0;
			for(String st : words)
			{
				boolean isTrue = countPresent(str,st); 
				if(isTrue)
				{
					no++;
				}
				
			}
			if(no > count)
			{
				count = no;
			}
		}
        return count;
    }
	public boolean countPresent(String s1 , String s2)
	{
		boolean isPresent = true;
		char[] ch1 = s1.toCharArray(), ch2 = s2.toCharArray();
		for (char c : ch1) 
		{
			for (char c1 : ch1) 
			{
				if(c1 != c)
				{
					isPresent = false;
					break;
				}
			}
		}
		return isPresent;
	}
}
