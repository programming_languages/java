package leetCode;

public class ReverseSentence 
{
	
	public static void main(String[] args) 
	{
		ReverseSentence rs = new ReverseSentence();
		String s = "Let's take LeetCode contest";
		rs.reverseWords(s);
	}
	public String reverseWords(String s) 
	{
		char[] ch = s.toCharArray();
		int space=0;
		for (int i = 0; i < ch.length-1; i++) 
		{
			if (ch[i] == ' ') 
	        {
	            reverse(space, i - 1, ch);
	            space = i + 1;
	        }
	    }
		reverse(space, ch.length - 1, ch);
		String ans = new String(ch);
		System.out.println(ans);
        return ans;
    }
	public void reverse(int i, int j, char[] ch) 
	{
		while(i<j)
		{	
	        char temp = ch[i];
	        ch[i] = ch[j];
	        ch[j] = temp;
	        i++;
	        j--;
		}
		
	}

}
