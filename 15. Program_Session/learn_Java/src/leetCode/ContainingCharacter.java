package leetCode;

import java.util.ArrayList;

public class ContainingCharacter 
{

	public static void main(String[] args) 
	{
		ContainingCharacter cc= new ContainingCharacter();
		String[] words = {"abc","bcd","aaaa","cbc"};
		char x = 'a';
		cc.findWordsContaining(words, x);

	}
	public ArrayList<Integer> findWordsContaining(String[] words, char x) 
	{
		ArrayList<Integer> ans = new ArrayList<Integer>();
		
		for(int i = 0;i<words.length;i++)
		{
			char[] ch = words[i].toCharArray();
			for(int j =0; j<ch.length;j++)
			{
				if(ch[j] == x)
				{
					ans.add(i);
					break;
				}
			}
		}
		System.out.println(ans);
        return ans;
    }

}
