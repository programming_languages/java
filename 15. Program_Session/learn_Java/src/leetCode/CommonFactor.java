package leetCode;

public class CommonFactor 
{

	public static void main(String[] args) 
	{
		CommonFactor cf =  new CommonFactor();
		int a = 25, b = 30;
		cf.commonFactors(a, b);
	}
	public int commonFactors(int a, int b) 
	{
		int no = a>b?a:b;
		int count =0;
		for(int i =1;i<=no;i++)
		{
			if(a % i == 0 & b % i == 0)
			{
				count ++;
			}
		}
		System.out.println(count);
		return count;
	}

}
