package leetCode;

public class CheckArrayString 
{

	public static void main(String[] args) 
	{
		CheckArrayString cas = new CheckArrayString();
		String[] word1 = {"abc", "d", "defg"};
		String[] word2 = {"abcddefg"};
		cas.arrayStringsAreEqual(word1, word2);
		
	}
	public boolean arrayStringsAreEqual(String[] word1, String[] word2) 
	{
		String name1 = "";
		String name2 = "";
		boolean isEqual = false; 
		for(int i =0;i<word1.length;i++)
		{
			name1 = name1+word1[i];
		}
		for(int i =0;i<word2.length;i++)
		{
			name2 = name2+word2[i];
		}
		if(name1.equals(name2))
		{
			isEqual=true;
		}
		System.out.println(isEqual);
        return isEqual;
    }

}
