package leetCode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RemoveDuplicate 
{

	public static void main(String[] args) 
	{
		RemoveDuplicate rd = new RemoveDuplicate();
		int[] nums = {0,0,1,1,1,2,2,3,3,4};
		System.out.println(rd.removeDuplicates(nums));

	}
	public int removeDuplicates(int[] nums) 
	{
		int count=1;

        for(int i=0;i<nums.length-1;i++){
        

          if(nums[i]!=nums[i+1]){
        	  	nums[count]=nums[i];
        	  	count++;
                nums[count] = nums[i+1];
            }
        }
        System.out.println(Arrays.toString(nums));
        return count;
		
	}

}
