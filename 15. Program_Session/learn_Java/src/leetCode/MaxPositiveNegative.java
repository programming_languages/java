package leetCode;

public class MaxPositiveNegative 
{

	public static void main(String[] args) 
	{
		MaxPositiveNegative mpn = new MaxPositiveNegative();
		int[] nums = {5,20,66,1314};
		System.out.println(mpn.maximumCount(nums));
		
	}
	public int maximumCount(int[] nums) 
	{
		int posCount = 0;
		int negCount = 0;
		for(int i =0; i<nums.length;i++)
		{
			if(nums[i] > 0 && nums[i] != 0)
			{
				posCount++;
			}
			else if(nums[i] < 0)
			{
				negCount++;
			}
		}
		if(posCount >= negCount)
		{
			return posCount;
		}
		return negCount;
    }

}
