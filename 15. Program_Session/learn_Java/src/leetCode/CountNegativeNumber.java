package leetCode;

public class CountNegativeNumber 
{

	public static void main(String[] args) 
	{
		CountNegativeNumber cnn = new CountNegativeNumber();
		int[][] grid = {{5,1,0},{-5,-5,-5}}; 
		cnn.countNegatives(grid);
		
	}
	 public int countNegatives(int[][] grid) 
	 {
		 	int count = 0;
		 	for(int i = 0;i<grid.length;i++)
		 	{
		 		for(int j = 0;j<grid[i].length;j++)
		 		{
		 			if(grid[i][j] < 0)
		 			{
		 				count++;
		 			}
		 		}
		 	}
		 	System.out.println(count);
	        return count;
	 }
}
