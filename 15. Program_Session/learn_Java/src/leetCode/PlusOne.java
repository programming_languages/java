package leetCode;

import java.math.BigInteger;

public class PlusOne 
{

	public static void main(String[] args) 
	{
		PlusOne po = new PlusOne();
		int[] digits = {1,2,3};
		po.plusOne(digits);

	}
	public int[] plusOne(int[] digits) 
	{
        String str = "";
        for(int i = 0;i<digits.length;i++)
        {
        	str = str + digits[i];
        }
        BigInteger no = new BigInteger(str);
        BigInteger no1 = BigInteger.valueOf(1);
        no = no.add(no1);
        String str1 = String.valueOf(no);
        int[] ar = new int[str1.length()];
        char[] ar1 = str1.toCharArray();
        for(int i =0;i<=ar1.length-1;i++)
        {
        	String s = String.valueOf(ar1[i]);
        	ar[i] =Integer.parseInt(s);
        }
        return ar;
        
    }

}
