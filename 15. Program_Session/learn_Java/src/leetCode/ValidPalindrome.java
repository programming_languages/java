package leetCode;

public class ValidPalindrome 
{

	public static void main(String[] args) 
	{
		ValidPalindrome vp = new ValidPalindrome();
		String s = "0P"; 
		System.out.println(vp.isPalindrome(s));

	}
	public boolean isPalindrome(String s) 
	{
		String str = s.toLowerCase();
		String ans = "";
		for(int i =0 ;i<str.length();i++)
		{
			if(str.charAt(i)>= 'a' && str.charAt(i)<='z' || (str.charAt(i)>= '0' && str.charAt(i)<='9'))
			{
				ans += str.charAt(i);
			}
		}
		String ans1 = "";
		for(int i = ans.length()-1;i>=0;i--)
		{
			ans1 += ans.charAt(i);
		}
		System.out.println(ans);
		System.out.println(ans1);
		if(ans.equals(ans1))
		{
			return true;
		}
	    return false;   
	}

}
