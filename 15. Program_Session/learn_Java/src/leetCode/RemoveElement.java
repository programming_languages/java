package leetCode;

import java.util.ArrayList;
import java.util.Arrays;

public class RemoveElement 
{

	public static void main(String[] args) 
	{
		RemoveElement re = new RemoveElement();
		int[] nums = {0,1,2,2,3,0,4,2};
		int val = 3;
		System.out.println(re.removeElement(nums, val));
		System.out.println(Arrays.toString(nums));
		
	}
	 public int removeElement(int[] nums, int val)
	 {
		 	int c=0;
	        for (int i = 0; i < nums.length; i++) {
	            if (nums[i]!=val) 
	            {
	                nums[c++]=nums[i];
	            }
	        }
	       return c;
	 }

}
