package leetCode;

public class InsertPosition 
{

	public static void main(String[] args) 
	{
		InsertPosition ip = new InsertPosition();
		int[] nums = {1,3,5,6};
		int target = 2;
		System.out.println(ip.searchInsert(nums, target));

	}
	public int searchInsert(int[] nums, int target) 
	{
        int count =1;
        while(count<nums.length)
        {
        	
        	if(nums[count]>=target)
        	{
        		return count;
        	}
        	count++;
        }
        return count;
	        
    }

}
