package leetCode;

public class PalindromeNumber 
{

	public static void main(String[] args) 
	{
		PalindromeNumber pn = new PalindromeNumber();
		int x = -121;
		pn.isPalindrome(x);
	}
	public boolean isPalindrome(int x) 
	{
		int no =0;
		int temp = x;
		boolean isPalindrome = false;
		if(x > 0)
		{
			
			while(x>0)
			{
				int rem = x%10;
				no =no*10 + rem;
				x /= 10;
			}
			if(no == temp)
			{
				isPalindrome = true;
			}
		}
		return isPalindrome;
	   
    }

}
