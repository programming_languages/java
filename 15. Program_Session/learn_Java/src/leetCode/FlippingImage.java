package leetCode;

public class FlippingImage 
{

	public static void main(String[] args) 
	{
		FlippingImage fi = new FlippingImage();
		int[][] image = {{1,1,0,0},{1,0,0,1},{0,1,1,1},{1,0,1,0}};
		fi.flipAndInvertImage(image);

	}
	public int[][] flipAndInvertImage(int[][] image) 
	{
		int[][] flip = new int[image.length][image[0].length];
		for(int i = 0;i<image.length;i++)
		{
			int n = image.length-1;
			for(int j = 0;j<image[i].length;j++)
			{
				flip[i][j] = image[i][n];
				n--;
			}
		}
		for(int i = 0;i<flip.length;i++)
		{
			
			for(int j = 0;j<flip[i].length;j++)
			{
				if(flip[i][j] == 0)
				{
					flip[i][j] = 1;
				}
				else
				{
					flip[i][j] = 0;
				}
			}
		}
	
        return flip;
    }

}
