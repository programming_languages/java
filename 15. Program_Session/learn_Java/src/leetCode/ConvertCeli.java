package leetCode;

public class ConvertCeli 
{

	public static void main(String[] args) 
	{
		ConvertCeli cc = new ConvertCeli();
		double celsius = 36.50d;
		cc.convertTemperature(celsius);
	}
	public double[] convertTemperature(double celsius) 
	{
	        double kelvin = celsius + 273.15;
	        double fahrenheit = celsius * 1.80 + 32.00;
	        double[] ans = new double[2];
	        ans[0] = kelvin;
	        ans[1] = fahrenheit;
	        for (double d : ans) {
				System.out.println(d);
			}
	        return ans;
	        
	}
}
