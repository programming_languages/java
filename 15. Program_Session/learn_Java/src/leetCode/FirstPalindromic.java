package leetCode;

public class FirstPalindromic {

	public static void main(String[] args) 
	{
		FirstPalindromic fp = new FirstPalindromic();
		String[] words = {"def","ghi"};
		System.out.println(fp.firstPalindrome(words));
		
	}
	public String firstPalindrome(String[] words) 
	{
		String ans = "";
		for(int i = 0; i<words.length;i++)
		{
			boolean isPalindrome = this.isPlaindrome(words[i]);
			if(isPalindrome)
			{
				ans = ans + words[i];
				break;
			}
		}
		return ans;
    }
	public boolean isPlaindrome(String name)
	{
		char[] namearr = name.toCharArray();
		
		String ans ="";
		boolean istrue = false;
		
		for(int i=namearr.length-1;i>=0;i--)
		{
			ans = ans+namearr[i];
		}
		if(ans.equals(name))
		{
			istrue =true;
		}
		return istrue;
	}
	

}
