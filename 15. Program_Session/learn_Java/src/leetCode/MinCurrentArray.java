package leetCode;

public class MinCurrentArray 
{

	public static void main(String[] args) 
	{
		MinCurrentArray mca = new MinCurrentArray();
		int[] nums = {8,1,2,2,3};
		mca.smallerNumbersThanCurrent(nums);
	}
	 public int[] smallerNumbersThanCurrent(int[] nums) 
	 {
		 int[] ans = new int[nums.length];
		 
		 for(int i = 0;i<nums.length;i++)
		 {
			 int count = 0;
			 for(int j = 0; j<nums.length;j++)
			 {
				 if(nums[i]>nums[j])
				 {
					 count++;
				 }
			 }
			 ans[i] = count;
		 }
		 for(int i = 0;i<ans.length;i++)
		 {
			 System.out.println(ans[i]);
		 }
		 
		 return ans;
	 }

}
