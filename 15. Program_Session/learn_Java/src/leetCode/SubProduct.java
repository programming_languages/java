package leetCode;

public class SubProduct 
{

	public static void main(String[] args) 
	{
		SubProduct sp = new SubProduct();
		int n = 4421;
		sp.subtractProductAndSum(n);
		

	}
	public int subtractProductAndSum(int n) 
	{
		int pro =1;
		int sum =0;
		while (n >0)
		{
			int rem = n %10;
			pro = pro*rem;
			sum = sum+rem;
			n /= 10;
		}
		int dif = pro - sum;
		System.out.println(dif);
		return dif;     
	}
}
