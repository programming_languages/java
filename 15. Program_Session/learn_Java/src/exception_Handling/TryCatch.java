package exception_Handling;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TryCatch 
{

	public static void main(String[] args) 
	{
		
		TryCatch tc = new TryCatch();
		
		//tc.calculate();
		//tc.getDetails();
		//tc.display();
		tc.findLCM();
		

	}
	
	public void display() 
	{
		int no1 = 12 , no2 =0 , no3 = -5;
		try 
		{
			System.out.println(no1/no2);
			int[] ar = new int[no3];
			
		}
		catch(ArithmeticException | NegativeArraySizeException e)
		{
			System.out.println(e.getMessage());
			System.out.println("Check Check Check");
		}
	}
	
	public void getDetails()
	{
		Scanner sc = new Scanner(System.in);
		
		try 
		{
			System.out.println("Enter Your Account Number: ");
			int accNo = sc.nextInt();
			System.out.println("Enter Your Pin Number: ");
			int pinNo = sc.nextInt();
			System.out.println("Enter The Length of the Array: ");
			//Nested Try Catch
			try 
			{
				int no = sc.nextInt();
				int[] empDetails = new int[no];
				for (int i = 0; i<empDetails.length ; i++)
				{
					empDetails[i] = 20000;
				}
			}
			catch(NegativeArraySizeException ne)
			{
				System.out.println("The Array Length Should be in Positive or Negative");
			}
			finally
			{
				System.out.println("Demo");
			}
			System.out.println(accNo/pinNo);
		}
		catch(ArithmeticException ae)
		{
			System.out.println(ae.getMessage()); 
		}
		catch(InputMismatchException ie)
		{
			System.out.println("Check The Account Number / PIN Number");
		}
		catch(IndexOutOfBoundsException io)
		{
			System.out.println("Check Array");
		}
		catch(Exception e)
		{
			System.out.println("Incorrect");
		}
		finally
		{
			System.out.println("The Transacation is closed.");
		}
		
	}
	
	public void calculate() throws Exception	 
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter two numbers:  ");
		int no1 = sc.nextInt();
		int no2 = sc.nextInt();
		
		System.out.println(no1/no2);
		
//		try 
//		{   
//			//Exception Possible Area
//			System.out.println(no1/no2);
//		}
//		catch(Exception e)
//		{
//			//Exception Handing Block
//			System.out.println("Check the divisors and corect it");
//			calculate();
//		}
		
		
		
	}
	public void findLCM()
	{
		int no = 1;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Three Numbers For Finding LCM: ");
		try
		{
			int no1 = sc.nextInt();
			int no2 = sc.nextInt();
			int no3 = sc.nextInt();
			while(true)
			{
				if(no/no1 == 0 && no/no2 == 0 && no/no3 == 0 )
				{
					System.out.println("The LCM is "+ no);
					break;
				}
				no++;
			}
		}
		catch(ArithmeticException ae)
		{
			System.out.println("Zero are not allowed");
			findLCM();
		}
		catch(InputMismatchException ie)
		{
			System.out.println("Your values are not vaild please check");
			findLCM();
		}
		catch(Exception e)
		{
			System.out.println("SomeThing Went Wrong");
			findLCM();
		}
		
		
	}

}
