package exception_Handling;

import java.util.Scanner;

public class Test 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Your UserName: ");
		String uname = sc.nextLine();
		
		System.out.println("Enter Your Password: ");
		String pwd = sc.nextLine();
		
		PasswordException pw = new PasswordException();
		pw.passwordVerification(pwd);
		
//		try {
//			pw.passwordVerification(pwd);
//		} catch (PasswordException e) {
//			System.out.println("Weak Password Not Accepted");
//		}

	}

}
