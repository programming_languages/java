package patternProgramming;

import java.util.Iterator;

/*
 *  *****
 *  *****
 *  *****
 *  *****
 *  *****
 *  
 *  */
public class Pattern1 
{

	public static void main(String[] args) 
	{
		int col;
		for(int row = 1; row <= 5;row++)
		{
			for(col =1;col<6-row;col++)
			{
				System.out.print("  ");
			}
			for(int stars =1;stars<=row;stars++)
			{
				System.out.print(" "+stars+"  ");
			}
			System.out.println();
		}
	}

}
