package patternProgramming;

import java.util.Scanner;

public final class Pattern2 
{

	public static void main(String[] args) 
	{
//		System.out.println("Enter No: ");
//		Scanner sc = new Scanner(System.in);
//		int no1 = sc.nextInt();
		for(int row = 1; row <=5; row++)
		{
			for(int col = row; col<5 ; col++)
			{
				System.out.print(" ");
			}
			for(int col2 = 1; col2<=row;col2++)
			{
				System.out.print(" "+row+" ");
			}
			System.out.println();
		}

	}

}
