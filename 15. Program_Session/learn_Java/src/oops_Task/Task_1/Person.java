package oops_Task.Task_1;

public class Person 
{
	//Task
	//Write a Java program to create a class called "Person" with a name and age attribute. 
	//Create two instances of the "Person" class, set their attributes using the constructor, and print their name and age. 
	
	String name;
	int age;
	
	public Person(String name, int age)
	{
		this.name = name;
		this.age = age;
	}
	public static void main(String[] args) 
	{
		
		Person person1 = new Person("Praba", 21);
		
		Person person2 = new Person("Karkuvel",22);
		
		System.out.println("Name: "+person1.name+" Age: "+person1.age);
		
		System.out.println("Name: "+person2.name+" Age: "+person2.age);

	}

}
