package java8Features.default_StaticMethods;

public interface Contract 
{
	//Default is Keyword Not Access Modifier
	
	public default void calculate(int no1, int no2)
	{
		System.out.println(no1+no2);
		
	}

	
}
