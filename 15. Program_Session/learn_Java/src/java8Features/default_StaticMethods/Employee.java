package java8Features.default_StaticMethods;

public class Employee implements Contract,Contract2
{

	public static void main(String[] args)
	{
//		Take Reference From Contract Interface and can Call Default Method
//		Contract c = new Employee();
		
//		And We can use the  interface default method by using object
		Employee c = new Employee();
		c.calculate(100, 200);

	}

	//Multiple Inheritance Achieved By using this Override method
	@Override
	public void calculate(int no1, int no2) 
	{
		
		Contract.super.calculate(no1, no2);
		Contract2.super.calculate(no1, no2);
	}

}
