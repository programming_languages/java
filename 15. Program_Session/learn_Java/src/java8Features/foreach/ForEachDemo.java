package java8Features.foreach;

import java.util.ArrayList;
import java.util.function.Consumer;

import array.Array;

public class ForEachDemo 
{

	public static void main(String[] args) 
	{
		ArrayList<Integer> al = new ArrayList<Integer>();
		al.add(20);
		al.add(30);
		al.add(40);
		al.add(50);
		al.add(60);
		
		//internal Loop foreach
		al.forEach((num)->System.out.println(num));//Passing Consumer
		
		Consumer<String> c = (str)->System.out.println(str);
		c.accept("praba");
		
		//External Loop
//		for (Integer num : al) {
//			System.out.println(num);
//			
//		}
//		for(int i =0;i<al.size();i++)
//		{
//			System.out.println(al.get(i));
//		}
	}

}
