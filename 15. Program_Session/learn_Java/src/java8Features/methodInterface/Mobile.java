package java8Features.methodInterface;

public class Mobile 
{
	int price ;
	Mobile(int price)
	{
		System.out.println("Constructor"+price);
	}
	public void show(int no)
	{
		System.out.println(no);
	}

	public static void main(String[] args) 
	{
//		Mobile m = new Mobile();
		
//		Refer the non static method using object
//		Contract c = m :: show;
		
//		Refer the static method using Class Name
//		Contract c = Mobile :: show;
		
//		Refer the Arbitrary Object
//		Contract c = new Mobile() :: show;
		
//		Refer the Constructor
//		Contract c = Mobile :: new; 
//		c.display(20000);
		
//		Contract c = (no) -> { System.out.println(no);};
//		c.display(120);
	}
}
