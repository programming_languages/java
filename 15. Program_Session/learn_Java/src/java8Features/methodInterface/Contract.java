package java8Features.methodInterface;

@FunctionalInterface
public interface Contract 
{
	public void display(int no);
}
