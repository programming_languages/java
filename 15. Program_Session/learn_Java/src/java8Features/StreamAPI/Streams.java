package java8Features.StreamAPI;

import java.util.ArrayList;

public class Streams 
{

	public static void main(String[] args) 
	{
		Employee emp1 = new Employee(1, "selva", 2);
		Employee emp2 = new Employee(2,"vignesh",5);
		Employee emp3 = new Employee(3,"monisha",1);
		
		ArrayList<Employee> al = new ArrayList<Employee>();
		al.add(emp1);
		al.add(emp2);
		al.add(emp3);
		
//		al.stream().map(name -> name.getName()).forEach(System.out::println);
		
		al.stream().filter(exp -> exp.getExp()>2).map(name -> name.getName()).forEach(System.out::println);

	}

}
