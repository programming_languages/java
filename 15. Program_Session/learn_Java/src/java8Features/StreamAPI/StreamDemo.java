package java8Features.StreamAPI;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.OptionalDouble;
import java.util.stream.IntStream;

public class StreamDemo 
{

	public static void main(String[] args) 
	{
		int[] ar = {101,200,300,400,500,600};
//		System.out.println(ar.length);
		
//		Arrays.stream(ar).skip(1).forEach(System.out :: println);
		
//		filter method is used to filter the array using condition 
//		Arrays.stream(ar).filter(no -> no%2!=0).forEach(System.out::println);
		
//		Arrays.stream(ar).map(no ->no/2).forEach(System.out::println);
		
//		map method is used to aplly the condition and give output
//		String[] ar1 =  {"karkuvel","Prabakaran"};
		
//		Arrays.stream(ar1).map(names  -> names.toUpperCase()).forEach(System.out::println);
		
		ArrayList<Integer> al = new ArrayList<Integer>();
		al.add(10);
		al.add(20);
		al.add(30);
		al.add(20);
		System.out.println(al);
		
//		al.stream().skip(2).forEach(System.out :: println);
		
		al.stream().limit(2).skip(1).forEach(System.out::println);
		
		
		/*
		 * IntStream s = Arrays.stream(ar); long count = s.count();
		 * System.out.println(count);
		 */
		
//		IntStream s1 = Arrays.stream(ar);
//		s1 = s1.sorted();
//		s1.forEach(System.out ::println);
		
		/*
		 * Arrays //ar - Collection of data .stream(ar) // Intermediate Process (Lazy
		 * Operation) .sorted() // Intermediate Process (Lazy Operation)
		 * .forEach(System.out :: println); //Terminal Process (Egar Operation)
		 * 
		 * OptionalDouble od = Arrays.stream(ar).average();
		 * System.out.println(od.isPresent()); System.out.println(od.getAsDouble());
		 */
		
		
		
		
	}


}
