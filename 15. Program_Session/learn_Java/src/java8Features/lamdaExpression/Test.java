package java8Features.lamdaExpression;

public class Test {

	public static void main(String[] args) 
	{
		
//		Rule rule = ()->{System.out.println("Hi");};
//		Rule rule = (no1,no2)->{System.out.println(no1+no2);};
		Rule rule = (no1,no2)->{return no1+no2;};
		rule.divide(); // call default method using reference 
//		int no = rule.add(20, 30);
		System.out.println(rule.add(50, 50));
//		rule.add(); // call Anonymous Function
		Rule.sub(); // call static method using Interface name

	}
	public void add()
	{
		System.out.println("Add Method");
	}

}
