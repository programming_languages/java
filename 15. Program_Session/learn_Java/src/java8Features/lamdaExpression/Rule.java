package java8Features.lamdaExpression;

@FunctionalInterface
public interface Rule 
{ 
	//Functional Interface Should have one abstract not more than one.
//	public  void add();
//	public void add(int no1,int no2);
	public int add(int no1,int no2);
	
	//we should declare method using default and static method
	//default call by using object
	//static call by using interface name
	public default void divide()
	{
		System.out.println("divide");
	}
	
	public static void sub()
	{
		System.out.println("sub");
	}
}
