public class Static_Variables
{
	static int age = 22; // Declare Static or Global Variables (class Variable)
	static String name = "Praba";
	public static void main(String [] args)
	{
		/* 
		Static or Global Variables (class Variable) can not be declare in method it only declare in class.

		*/	
		name = Static_Variables.name; // Assigning the static variable value to local variable
		age = Static_Variables.age;
		
		/*Print Local Variable*/
		System.out.println(name);
		System.out.println(age);
		
		/* Print Static or Global Variables (class Variable) Using Class Name*/
		System.out.println("Print Global Variables by using Class Name "+Static_Variables.name);
		System.out.println("Print Global Variables by using Class Name "+Static_Variables.age);
		
		Static_Variables object = new Static_Variables(); // creating object 
		
		/* Print Static or Global Variables (class Variable) using Object name*/
		System.out.println("Print Global Variables by using Object "+object.name);
		System.out.println("Print Global Variables by using Object "+object.age);
		
		/* Print Static or Global Variables (class Variable) call Directly*/
		System.out.println("Print Static Variables (class Variable) call Directly "+name);
		System.out.println("Print Static Variables (class Variable) call Directly "+age);
		
	}
}