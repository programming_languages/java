public class Default_Values
{
	// Declaring Static Variable or Class Variable
	static String name;
	static byte id;
	static short age;
	static int rollNo;
	static long mob;
	static float weight;
	static double height;
	static char initial;
	static boolean isStudent;
	
	//Declaring Non-Static Variables or object Variables
	String name1;
	byte id1;
	short age1;
	int rollNo1;
	long mob1;
	float weight1;
	double height1;
	char initial1;
	boolean isStudent1;
	public static void main(String [] args)
	{
		// Default Values for only field (Global Variable) Not for local Variable
		
		System.out.println("****** PRINTING STATIC VARIABLES ******");
		System.out.println("Default Values for String " + Default_Values.name);
		System.out.println("Default Values for Byte " + Default_Values.id);
		System.out.println("Default Values for Short " + Default_Values.age);
		System.out.println("Default Values for Int " + Default_Values.rollNo);
		System.out.println("Default Values for Long " + Default_Values.mob);
		System.out.println("Default Values for Float " + Default_Values.weight);
		System.out.println("Default Values for Double " + Default_Values.height);
		System.out.println("Default Values for Char " + Default_Values.initial);
		System.out.println("Default Values for Boolean " + Default_Values.isStudent);
		
		Default_Values object = new Default_Values(); // Creating Object 
		
		System.out.println("****** PRINTING NON-STATIC VARIABLES ******");
		System.out.println("Default Values for String " + object.name1);
		System.out.println("Default Values for Byte " + object.id1);
		System.out.println("Default Values for Short " + object.age1);
		System.out.println("Default Values for Int " + object.rollNo1);
		System.out.println("Default Values for Long " + object.mob1);
		System.out.println("Default Values for Float " + object.weight1);
		System.out.println("Default Values for Double " + object.height1);
		System.out.println("Default Values for Char " + object.initial1);
		System.out.println("Default Values for Boolean " + object.isStudent1);
	}
		
}