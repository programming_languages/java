public class Instance_Variables 
{
	String name = "Praba"; //Declare Non-Static or Global Variables (object Variable)
	int age = 23; //Declare Non-Static or Global Variables (object Variable)

	public static void main(String [] args)
	{
		 
		Instance_Variables object = new Instance_Variables();//object creating
		
		
		String name = object.name;// Assigning the Non-static variable value to local variable
		int age = object.age;
		
		// printing local variables
		System.out.println("The Name is " +name);
		System.out.println("The Age is " +age); 
		
		//printing instance varibles using object
		System.out.println("The Name is " +object.name);
		System.out.println("The Age is " +object.age);
		
		
	}
}