public class Variables
{
	// global(static) variable  OR class variables
	static String name = "Adithiya";
	static int age = 15;
	String name2 = "Monish"; // non-static variable or instance variable or object variable
	public static void main(String []args)
	{
		// local variable 	
		String name = "Praba"; 
		int age = 12;
		name = "Karkuvel";
		// String name = "xyz" it will make error
		
		// print global (static) variable
		System.out.println(Variables.name);
		System.out.println(Variables.age);
		
		//print local variable
		System.out.println(name);
		System.out.println(age);
	}
}
