import com.javaProgram.MaximumNumber; // using import keyword to use the user-build package

public class Number
{
	public static void main(String [] args)
	{
		/* 
			using the package by creating object using the class name of the package 
		*/
		MaximumNumber max = new MaximumNumber(22,33,44); // with three parameters
		
		max.maxNumber();//call the method which method is inside the package
	}
}